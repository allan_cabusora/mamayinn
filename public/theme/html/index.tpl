<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>MAMAY INN SYSTEM</title>

	<!-- Bootstrap Core CSS -->
	<link href="{SITEURL}theme/assets/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="{SITEURL}theme/assets/css/sb-admin.css" rel="stylesheet">

	<link href="{SITEURL}theme/assets/css/daterangepicker.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="{SITEURL}theme/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" type="text/css" href="{SITEURL}theme/css/jquery-ui-1.11.2/jquery-ui.min.css">
	<link href="{SITEURL}theme/assets/css/jquery.loadmask.css" rel="stylesheet" type="text/css" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- jQuery -->
	<script src="{SITEURL}theme/assets/js/jquery.js"></script>
	<script src="{SITEURL}theme/assets/js/moment.min.js"></script>


	<script type="text/javascript" src="{SITEURL}js/jquery-ui.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="{SITEURL}theme/assets/js/bootstrap.min.js"></script>

	<script src="{SITEURL}theme/assets/js/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="{SITEURL}theme/assets/js/jquery.loadmask.min.js"></script>

	<script src="{SITEURL}theme/assets/js/daterangepicker.js"></script>
	<script type="text/javascript">
		$(function() {


			$('.datepicker-range').daterangepicker({
				/*timePicker: true,
				timePickerIncrement: 30,*/
				ranges: {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				},
				locale: {
					format: 'MM/DD/YYYY'
				}
			});

			$('.datepicker').daterangepicker({
				singleDatePicker: true,
				showDropdowns: true
			});


		});


	</script>
</head>

<body>

<div class="alert alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<div class="message"></div>
</div>

<div id="wrapper">

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{SITEURL}">Mamay INN Admin Panel</a>
		</div>
		<!-- Top Menu Items -->
		<ul class="nav navbar-right top-nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {USERNAME} <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li>
						<a href="{SITEURL}auth/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
					</li>
				</ul>
			</li>
		</ul>

		<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav side-nav">
				<li class="active">
					<a href="{SITEURL}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
				</li>
				<li>
					<a href="{SITEURL}transaction/my-summary"><i class="fa fa-fw fa-money"></i> Transaction</a>
				</li>

				{ADMIN_MENU}

			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</nav>

	{CONTENTS}
	<!-- /#page-wrapper -->

</div>



</body>

</html>
