$(document).ready(function(){

    $("#charges").change(function(){
        var rm_id =  $(this).parents('form').find("input[name=rm_id]").val();
        var rt_id =  $(this).parents('form').find("input[name=rt_id]").val();
        var ec_id =  $(this).parents('form').find("input[name=ec_id]").val();

        getExtraCharge(rm_id, rt_id, $(this).val(), 0);
    });

    $("#extra_charge_item").change(function(){
        var rm_id =  $(this).parents('form').find("input[name=rm_id]").val();
        getExtraChargeDetails(rm_id, $(this).val());
    });

    $("#qty").keyup(function(){
        calcTotalCharges();
    });

});

function calcTotalCharges3(rate) {
    var tot = parseInt( rate ) * parseInt($("#qty").val());
    $("#rate").val(tot.toFixed(2));
}

function calcTotalCharges(qty) {

    var tot = parseInt( $("#qty").val() ) * parseInt($("#charge_rate").val());

    if(isNaN(tot)){
        $("#rate").val("0.00");
    }else{
        $("#rate").val(tot.toFixed(2));
    }

}

function getExtraCharge(rm_id, rt_id, cat_id, ec_id){

    $.ajax({
        url: '/manage/charges/extra-charges',
        type: 'GET',
        data: {
            id: cat_id,
            rm_id: rm_id,
            rt_id: rt_id,
            ec_id: ec_id
        },
        success: function(result){

            var options = $("#extra_charge_item");
            options.empty();
            options.append($("<option />").val("").text("Charge Item"));

            console.log(ec_id);
            $.each(result.data, function() {
                var opt = $("<option />");
                opt.val(this.ec_id).text(this.ec_name);
                if(ec_id == this.ec_id){
                    opt.prop('selected', true);
                }

                options.append(opt);
            });


            getExtraChargeDetails(rm_id, ec_id);
        }
    });
}

function getExtraChargeDetails(rm_id, ec_id){

    $.ajax({
        url: '/manage/charges/check-charges',
        type: 'GET',
        data: {
            id: ec_id,
            rm_id: rm_id
        },
        success: function(data){
            $("#charge_rate").val(data.ec_price);
            calcTotalCharges(data.ec_price);
        }
    });
}