$(document).ready(function(){

    $('#grid_charges').grid({}, function(event, data){
        var rows = data['data'];
        var total = 0;
        $.each(rows,function(key, value){
            var orderLine = value.rc_qty * value.rc_rate;
            total += orderLine;
        });

        $("#extra_charge").val(total.toFixed(2));

        if(!event.getInitialLoad()){
            $("#grid_charges").parent('form').find('input[name="reload_grid"]').val('true');
        }

        event.setInitialLoad(false);

        setTimeout(splitHour, 500);
        setTimeout(calcTotal, 500);

    });


    $("select[name=room]").change(function(){
        $.ajax({
            url: '/manage/checked-in/check-room',
            type: 'GET',
            data: {
                id: $(this).val()
            },
            success: function(data){
                //$("#room_type").val(data.rtp_name);
                $("#room_type").text(data.rtp_name + ' ( ' + data.rtp_hourly_rate + ' / hr )');
                $("#room_charge").val(0);

                $("#hourly_rate").val(data.rtp_hourly_rate);

                calcTotal();

            }
        });
    });

    $("#room-rates").change(function(){
        splitHour();
        calcTotal();
    });

    //$("#checkout").unbind("click");
    $("#checkout").on('click', function(){
        var rt_id = $(this).data('room');
        var ext_hours = $(this).parent().find('#rt_extended_hours').val();
        if(confirm("Are you sure??")){
            $.ajax({
                url: '/manage/checked-in/out',
                type: 'POST',
                data: {
                    id: rt_id,
                    ext_hours: ext_hours
                },
                success: function(data){
                    openReceipt(rt_id);
                }
            });
        }
    });

    //$("#checkout-cancel").unbind("click");
    $("#checkout-cancel").on('click', function(){
        var rt_id = $(this).data('room');
        if(confirm("Are you sure??")){
            $.ajax({
                url: '/manage/checked-in/cancel',
                type: 'POST',
                data: {
                    id: rt_id
                },
                success: function(data){
                    window.location.reload();
                }
            });
        }
    });

    var timer;
    $("#promo_code").on('keyup', function(){
        if($(this).val().length > 3){
            var that = this;
            clearTimeout (timer);
            suggest_timer = function(){
                $.ajax({
                    url: '/manage/checked-in/check-promo',
                    type: 'POST',
                    data: {
                        'code': $(that).val()
                    },
                    success: function(data){
                        if(data != false){
                            if(data.prm_discount > 0){
                                $("#promo_disc").val(data.prm_discount);
                                $("#promo_msg").html("Discounted by " + data.prm_discount);
                            }else{
                                $("#promo_disc_percentage").val(data.prm_discount_percentage);
                                $("#promo_msg").html("Discounted by " + data.prm_discount_percentage * 100 + "%");
                            }
                            $("#promo_msg").css('background', 'green');
                            $("#promo_id").val(data.prm_id);
                            calcTotal();
                        }else{
                            $("#promo_msg").css('background', 'red');
                            $("#promo_msg").html("Invalid Promo Code");

                            $("#promo_disc").val(0);
                            $("#promo_disc_percentage").val(0);
                            $("#promo_id").val('0');
                        }
                        $("#promo_msg").css('display','block');
                        calcTotal();
                    }
                });
            };

            timer = setTimeout(suggest_timer, 500);
        }else{
            $("#promo_msg").hide();

            $("#promo_disc").val(0);
            $("#promo_disc_percentage").val(0);
            $("#promo_id").val('0');
            calcTotal();
        }
    });
    var timer;

    $(".hp").on('click',function(){
        clearTimeout(timer);

        var cur_hour = parseInt( $("#rt_extended_hours").val() );

        new_hour = cur_hour + 1;

        $("#rt_extended_hours").val(new_hour);

        timer = setTimeout(calcTotal, 1000);
    });

    $(".hm").on('click',function(){
        clearTimeout(timer);

        var cur_hour = parseInt( $("#rt_extended_hours").val() );

        if(cur_hour <= 0){
            new_hour = 0;
        }else{
            new_hour = cur_hour - 1;
        }
        $("#rt_extended_hours").val(new_hour);

        timer = setTimeout(calcTotal, 1000);
    });



    if($("#time_elapsed").data('countdown') != ""){
        var $this = $("#time_elapsed"), finalDate = $("#time_elapsed").data('countdown');
        var loadTime = false;
        $this.countdown(finalDate, {elapse: true})
            .on('update.countdown', function(event) {
                var $this = $(this);

                var totalHours = event.offset.totalDays * 24 + event.offset.hours;

                var day = Math.floor(totalHours / 24);
                console.log(day);
                var day_str = "";
                if(day > 0){
                    day_str = day + " Days ";
                }

                if (event.elapsed) {
                    var min = parseInt( event.strftime('%M') );
                        if(event.offset.minutes == 28 && event.offset.seconds == 0){
                            calcTotal();
                        }
                    $this.html(event.strftime(day_str + ' %H:%M:%S'));
                }

                if(!loadTime && $("#room-rates").val() == 0){
                    splitHour();
                    loadTime = true;
                }

            });
    }

});

function splitHour(){
    var room_rates = $("#room-rates").val();
    var extended_hr = parseInt($("#rt_extended_hours").val());

    var hrs = 0;
    var rr = 0;
    //var rr = parseFloat( $("#rtp_hourly_rate").val() );

    if(room_rates > 0){
        rr_text = $("#room-rates option:selected").text().split(" - ");
        rr = parseFloat(rr_text[1]);
        tmp = rr_text[0].split(" ");
        hrs = parseInt(tmp[0]);
    }

    $("#rt_hours").val(hrs);
    $("#rt_rr_rate").val(rr);

    var running_hour = getRunningHour();
    
    if(running_hour >= (hrs + extended_hr)){
        var ext_hrs = running_hour - hrs;
        $("#rt_extended_hours").val(ext_hrs);
    }else{
        $("#rt_extended_hours").val(extended_hr);
    }
}

function calcTotal() {
    roomCharge();
    var extra_charge = parseInt($('#extra_charge').val());
    var room_charge = parseFloat($("#room_charge").val());
    var promo = 0;

/*

    if($("#promo_disc").val() > 0){
        room_charge = room_charge - parseInt($("#promo_disc").val());
    }else if($("#promo_disc_percentage").val() > 0){
        var disc_per = room_charge * (parseInt($("#promo_disc_percentage").val()) / 100 );
        room_charge = room_charge - disc_per;
    }
*/

    var tot = extra_charge + room_charge;

    $("#total_charge").val(tot.toFixed(2));
}

function roomCharge(){

    var ext_hrs = 0;
    var hrs = $("#rt_hours").val();

    if(getRunningHour() == 0 && hrs == 0){
        $("#rt_extended_hours").val(1)
    }

    if( $("#rt_extended_hours").val() ){
        ext_hrs = parseFloat($("#rt_extended_hours").val());
    }

    var rt_rr_rate = $("#rt_rr_rate").val();
    var rtp_hourly_rate = parseFloat($("#rtp_hourly_rate").val());

    var tot_rate = 0;

    if(hrs > 0){
        tot_rate += parseFloat( rt_rr_rate );

        if($("#promo_disc_percentage").val() != 0){
            var disc = parseFloat( rt_rr_rate ) * parseFloat($("#promo_disc_percentage").val());
            tot_rate -= disc;
        }
    }
    //else{
    //    tot_rate = rtp_hourly_rate;
    //}

    if(ext_hrs > 0){
        tot_rate += (ext_hrs * rtp_hourly_rate);
    }

    var exceed_hour = checkHour(true);
    if(exceed_hour > 0){
        tot_rate += exceed_hour * rtp_hourly_rate;
    }

    $("#room_charge").val(tot_rate.toFixed(2));

}

// Calculates the Initial Hour and Extended Hour
function getHour(){

    var def_hour = parseInt( $("#rt_hours").val() );
    var ext_hour = 0;

    if( $("#rt_extended_hours").val() ){
        ext_hour = parseInt( $("#rt_extended_hours").val() );
    }

    var consumed_hour = def_hour + ext_hour;

    return consumed_hour;

}



function getRunningHour(){

    if( $("#time_elapsed").text() ){

        var time_str = $("#time_elapsed").text()

        var days = time_str.split('Days');
        var days_to_hour = 0;
        var time = days[0];

        if(days.length > 1){
            days_to_hour = parseInt(days[0]) * 24;
            time = days[1];
        }

        var consumed_time = time.split(":");
        var consumed_hour = parseInt(consumed_time[0]);
        if(parseInt(consumed_time[1]) > 27){ // Flat Rate: if exceed 27 mins considered as 1 hr
            consumed_hour++;
        }

        return consumed_hour + days_to_hour;

    }else{
        return 0;
    }

}

function checkHour( exceed){
    var ex_hour = 0;
    if(typeof exceed == undefined)
        exceed = false;

    var consumed_hour = getHour();
    if( getRunningHour() > consumed_hour){
        //$("#over_hour").show();
        ex_hour = getRunningHour() - consumed_hour;
        //$("#over_hour").text("Exceeds " + ex_hour + " hours");
        //$("#rt_extended_hours").val(ex_hour);
        //console.log(ex_hour);
    }else{
        $("#over_hour").hide();
    }


    if(exceed){
        return ex_hour;
    }
}