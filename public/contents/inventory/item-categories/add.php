<?php

$cat_id = (isset($_GET['cat_id'])) ? $_GET['cat_id'] : 0;
$_contents->setVariable('cat_id', $cat_id);


$status = array('Disable', 'Activate' );

foreach($status as $key=>$value){
    $_contents->setCurrentBlock('promo-status');
    $_contents->setVariable('status_id', $key);
    $_contents->setVariable('status_label', $value);
    $_contents->parseCurrentBlock();
}