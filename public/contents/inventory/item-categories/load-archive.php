<?php

$id = (isset($_GET['cat_id'])) ? $_GET['cat_id'] : 0;

$sql = 'SELECT
        *,
        IF(ec_status=1,"<span class=\"label label-success\">Active</span>","<span class=\"label label-danger\">In-Active</span>") as status
        FROM
        tbl_extra_charges
        WHERE ec_deleted = 1 AND ec_cat_id = ' . $id . '
        ORDER BY ec_name ASC';

$paging = ['currPage'=>$_GET['page'], 'recPerPage'=>10];             // optional parameter to set the returns in pages
$records = $qry->getRecords($sql, $paging);                              // returns associative array [info, data]

$app->json_encode($records);
