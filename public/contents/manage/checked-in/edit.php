<?php

$rm_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$rt_id = (isset($_GET['rt_id'])) ? $_GET['rt_id'] : 0;

if($_SESSION['usertype'] == 9){
    $_contents->setVariable('checkoutcancel', '<button class="btn btn-danger btn-lg btn-block " id="checkout-cancel" data-room="' . $rt_id . '">Cancel</button>');
}else{
    $_contents->setVariable('DISABLED', 'disabled');
    $_contents->setVariable('RESTRICTED', 'restricted');

}

$_contents->setVariable('rt_id', $rt_id);
$_contents->setVariable('room_id', $rm_id);

$sql = "SELECT * FROM tbl_room_transactions
        LEFT JOIN tbl_rooms ON rt_room_id = rm_id
        LEFT JOIN tbl_room_rates ON rt_rr_id = rr_id
        LEFT JOIN tbl_room_types ON rm_type = rtp_id
        LEFT JOIN tbl_promos ON rt_promo_id = prm_id
        WHERE rt_id = " . $rt_id;

$rt = $qry->getRecords($sql);

$rm_type_id = 0;
$rr_id = 0;
$rt_vehicle = 0;
$rm_day = date('N');

foreach($rt['data'] as $row){

    if(!$row['rt_hours']){
        $row['rt_hours'] = '0';
    }

    $_contents->setVariable($row);
    $rm_type_id = $row['rtp_id'];
    $rr_id = $row['rr_id'];
    $rm_day = $row['rr_room_day'];
    $rt_vehicle = $row['rt_v_id'];

    if($row['prm_id'] != null){
        $_contents->setVariable('prm_disable', 'readonly');
        $_contents->setVariable('prm_show_msg', 'display: block;background:green;');
        if($row['prm_discount'] > 0 && $row['prm_discount'] != null){
            $_contents->setVariable('prm_msg', 'Discounted by ' . $row['prm_discount']);
        }else{
            $disc = $row['rr_rate'] * ($row['prm_discount_percentage'] / 100);
            $tot_disc = $row['rr_rate'] - $disc;
            $_contents->setVariable('prm_msg', 'Discounted by ' . $row['prm_discount_percentage'] . '%');
        }
    }
}


$sql = "SELECT * FROM tbl_rooms ORDER BY rm_number ASC";
$rooms = $qry->getRecords($sql);
foreach($rooms['data'] as $row){
    $_contents->setCurrentBlock('rooms');
    $_contents->setVariable($row);
    if($row['rm_status'] != 1 && $row['rm_number'] != $rm_id){
        $_contents->setVariable('rm_disabled', 'disabled');
    }

    if($row['rm_id'] == $rm_id) $_contents->setVariable('rm_selected', 'SELECTED');

    $_contents->parseCurrentBlock();
}


$sql = "SELECT * FROM tbl_room_rates
        INNER JOIN tbl_room_types ON rr_room_type = rtp_id
        WHERE rr_room_type = {$rm_type_id}   AND rr_room_day = {$rm_day}
        ORDER BY rr_hours ASC";
$room_rates = $qry->getRecords($sql);
foreach($room_rates['data'] as $row){
    $_contents->setCurrentBlock('room_rates');
    $_contents->setVariable($row);
    if($row['rr_id'] == $rr_id) $_contents->setVariable('rr_selected', 'SELECTED');

    $_contents->parseCurrentBlock();
}


/*
$sql = "SELECT * FROM tbl_room_rates
        INNER JOIN tbl_room_types ON rr_room_type = rtp_id
        WHERE rtp_id = {$rm_type_id}
        ORDER BY rtp_name ASC";
$room_rates = $qry->getRecords($sql);

foreach($room_rates['data'] as $row){
    $_contents->setCurrentBlock('room_types');
    $_contents->setVariable($row);
    if($row['rr_id'] == $rr_id){
        $_contents->setVariable('rm_selected', 'SELECTED');
    }

    $_contents->parseCurrentBlock();
}*/




$sql = "SELECT * FROM tbl_vehicles";
$room_rates = $qry->getRecords($sql);
foreach($room_rates['data'] as $row){
    $_contents->setCurrentBlock('vehicles');
    $_contents->setVariable($row);
    if($row['v_id'] == $rt_vehicle) $_contents->setVariable('v_selected', 'SELECTED');

    $_contents->parseCurrentBlock();
}
