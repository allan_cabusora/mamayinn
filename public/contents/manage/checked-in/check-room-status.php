<?php

$id = ($_GET['id']) ? $_GET['id'] : 0;


$sql = "SELECT * FROM tbl_rooms
        LEFT JOIN tbl_room_transactions ON rt_status = 1 AND rt_room_id = rm_id
        LEFT JOIN tbl_room_transaction_charges ON rc_room_transaction = rt_id AND rc_ec_id = 1
        WHERE rm_id = {$id} AND rm_status = 1
        GROUP BY rm_id
        ORDER BY rm_status DESC, rt_checked_in ASC, rm_number ASC";
echo $sql;
$room = $qry->getRecords($sql);

$room_details = array();

foreach($room['data'] as $row){

    $room_details['rm_number'] = sprintf("%03d", $row['rm_number']);
    $extra_time = ($row['rc_qty']) ? $row['rc_qty'] : 0;

    $room_stat = 'available';
    $new_date = date('Y-m-d H:i:s', strtotime('+' . $row['rt_hours']+$extra_time . ' hours', strtotime($row['rt_checked_in'])));
    if($row['rt_id'] && $row['rt_status'] == 1 && $row['rm_status'] == 1){
        $room_stat = 'occupied';
        $room_details['rm_time'] = $new_date;

        if(date('Y-m-d H:i:s') > $new_date){
            $room_stat = "end";
        }
        $room_details['action'] = 'edit';
    }else{
        $room_details['action'] = 'add';
    }

    $room_details['rt_id'] = ($row['rt_id'] && $row['rt_status'] == 1 && $row['rm_status'] == 1) ? $row['rt_id'] : -1;
    $room_details['rm_id'] = $id;

    $room_details['color_status'] = $room_stat;
    $room_details['rm_status'] = $room_stat;

}


if($room_details){
    $app->json_encode($room_details);
}else{
    echo 0;
}


