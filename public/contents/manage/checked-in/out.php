<?php

$id = isset($_POST['id']) ? $_POST['id'] : 0;
$sql = "SELECT rt_room_id FROM tbl_room_transactions WHERE rt_id = " . $id;

$rt = $qry->getRecords($sql);
$rm_id = 0;
$ext_hrs = isset($_POST['ext_hours']) ? $_POST['ext_hours'] : 0;
foreach($rt['data'] as $row){
    $rm_id = $row['rt_room_id'];
}

$sql = 'UPDATE tbl_room_transactions SET
        rt_status = 1,
        rt_extended_hours = ' . $ext_hrs . ',
        rt_checked_out = now(),
        rt_checked_out_by = ' . $_SESSION['u_id'] . '
        WHERE rt_id = ' . $id;

$qry->exec($sql);

$sql = 'UPDATE tbl_rooms SET
        rm_status = 1
        WHERE rm_id = ' . $rm_id;

$qry->exec($sql);

/*
// INSERT TO THE tbl_reports
$sql = 'SELECT
            rt.rt_id,
            rm_number,
            rtp_name,
            rt_date_created,
            rt_checked_in as check_in,
            rt_checked_out as check_out,
            if(rt.rt_hours > th.hours, rt.rt_hours, th.hours) as hours,
            COALESCE( SUM(rc_qty * rc_rate), 0 ) as extra_charges,
            COALESCE( (IF(rt.rt_hours > th.hours, rt.rt_hours, th.hours) - COALESCE(MAX(rr_hours), 0) ) * COALESCE(rtp_hourly_rate,0) + COALESCE( MAX(rr_rate),0 ) , 0 ) as room_charge,
            COALESCE( SUM(rc_qty * rc_rate), 0 ) + COALESCE( (IF(rt.rt_hours > th.hours, rt.rt_hours, th.hours) - COALESCE(MAX(rr_hours), 0) ) * COALESCE(rtp_hourly_rate,0) + COALESCE( MAX(rr_rate),0 ) , 0 ) as total_charge
        FROM
        tbl_room_transactions as rt
        LEFT JOIN (
            SELECT trt.rt_id, IF(MINUTE( TIMEDIFF(rt_checked_out, rt_checked_in)) < 27, HOUR(TIMEDIFF(rt_checked_out, rt_checked_in)), HOUR(TIMEDIFF(rt_checked_out, rt_checked_in))+1) as hours
            FROM tbl_room_transactions trt
        ) th ON th.rt_id = rt.rt_id
        LEFT JOIN tbl_rooms ON rt_room_id = rm_id
        LEFT JOIN tbl_room_types ON rm_type = rtp_id
        LEFT JOIN tbl_room_transaction_charges ON rc_room_transaction = rt.rt_id
        LEFT JOIN tbl_room_rates ON rr_room_type = rm_type AND rr_hours <= IF(rt.rt_hours > th.hours, rt.rt_hours, th.hours)
        WHERE rt.rt_id = ' . $id . '
        GROUP BY rc_room_transaction, rr_room_type';


$records = $qry->getRecord($sql);

$sql = 'INSERT INTO tbl_reports SET
            rpt_rm_number = ' . $records['rm_number'] . ',
            rpt_rm_type = "' . $records['rtp_name'] . '",
            rpt_checked_in = "' . $records['check_in'] . '",
            rpt_checked_out = "' . $records['check_out'] . '",
            rpt_hours = ' . $records['hours'] . ',
            rpt_room_charge = ' . $records['room_charge'] . ',
            rpt_extra_charge = ' . $records['extra_charges'] . ',
            rpt_rt_id = ' . $records['rt_id'] . ',
            rpt_user_id = ' . $_SESSION['u_id'] . '
          ';

$qry->exec($sql);*/


