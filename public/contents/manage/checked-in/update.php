<?php
$post = $_POST;                                            // get $_POST variables
$type = $post['type'];
$action = $post['action'];

/*
$sql = 'SELECT * FROM tbl_room_rates
        INNER JOIN tbl_room_types ON rr_room_type = rtp_id
        WHERE rr_id = ' . $post['room_types'];

$room_types = $qry->getRecord($sql);*/

$v_id = ($post['v_id']) ? $post['v_id'] : 0;
$hours = ($post['rt_hours']) ? $post['rt_hours'] : '0';

$rt_id = -1;

if($action == 'add'){

        $ext_hours = $post['rt_extended_hours'];

        if($post['room_rates'] == 0){
            $ext_hours = 0;
        }

        $check_out = ($post['room'] == 999) ? 'now()' : 'NULL';
        $check_out_by = ($post['room'] == 999) ? $_SESSION['u_id'] : 'NULL';

        $sql = 'INSERT INTO tbl_room_transactions SET
        rt_room_id = ' . $post['room'] . ',
        rt_hours = ' . $hours . ',
        rt_extended_hours = ' . $ext_hours . ',
        rt_customer_name = "' . $post['customer_name'] . '",
        rt_checked_in = now(),
        rt_checked_out = ' . $check_out . ',
        rt_date_created = now(),
        rt_checked_out_by = ' . $check_out_by . ',
        rt_hourly_rate = ' . $post['rtp_hourly_rate'] . ',
        rt_rr_rate = ' . $post['rt_rr_rate'] . ',
        rt_rr_id = ' . $post['room_rates'] . ',
        rt_user_id = ' . $_SESSION['u_id'] . ',
        rt_promo_id = ' . $post['promo_id'] . ',
        rt_promo_code = "' . $post['promo_code'] . '",
        rt_promo_rate = ' . $post['discount_percentage'] . ',
        rt_v_id = ' . $v_id . ',
        rt_status = 0
      ';

        $rt = $qry->exec($sql);

        $rt_id = $rt['lastid'];

        $sql = "UPDATE tbl_promos SET prm_status = 0 WHERE prm_id = " . $post['promo_id'];
        $rts = $qry->exec($sql);

        if(isset($_SESSION['charges_' . $post['room']])){

                $charges = $_SESSION['charges_' . $post['room']];
                foreach($charges as $charge){
                        $sql = "INSERT INTO tbl_room_transaction_charges SET
                                rc_room_transaction = " . $rt['lastid'] . ",
                                rc_ec_id = " . $charge['ec_id'] . ",
                                rc_description = '" . $charge['ec_description'] . "',
                                rc_rate = " . $charge['rc_rate'] . ",
                                rc_qty = " . $charge['rc_qty'];

                        $qry->exec($sql);
                }
        }

    $sql = "UPDATE tbl_rooms SET rm_status = 0 WHERE rm_id = " . $post['room'];
    $qry->exec($sql);

    $rs = $qry->resultMsg;
    $rs['params'] = '?id=' . $post['room'] .'&rt_id='.$rt_id;
    $rs['action'] = 'add';
    $rs['id'] = $rt_id;

    $app->json_encode($rs);
}else{

    $sql = "SELECT * FROM tbl_room_transactions WHERE rt_id = " . $post['id'];
    $prev_records = $qry->getRecord($sql);

    $ext_hours = $post['rt_extended_hours'];
    $rr_rate = $post['rt_rr_rate'];

    if($post['room_rates'] == 0){
        $ext_hours = 0;
    }

        $sql = 'UPDATE tbl_room_transactions SET
        rt_room_id = ' . $post['room'] . ',
        rt_hours = ' . $hours . ',
        rt_extended_hours = "' . $ext_hours . '",
        rt_customer_name = "' . $post['customer_name'] . '",
        rt_hourly_rate = ' . $post['rtp_hourly_rate'] . ',
        rt_rr_rate = ' . $post['rt_rr_rate'] . ',
        rt_rr_id = ' . $post['room_rates'] . ',
        rt_modified_by = ' . $_SESSION['u_id'] . ',
        rt_promo_id = ' . $post['promo_id'] . ',
        rt_promo_code = "' . $post['promo_code'] . '",
        rt_promo_rate = ' . $post['discount_percentage'] . ',
        rt_v_id = ' . $v_id . ',
        rt_date_modified = now(),
        rt_status = 0
        WHERE rt_id = ' . $post['id'];

        $rt = $qry->exec($sql);



        $sql = "UPDATE tbl_promos SET prm_status = 0 WHERE prm_id = " . $post['promo_id'];
        $rts = $qry->exec($sql);

        if($prev_records['rt_room_id'] != $post['room']){
            $sql = "UPDATE tbl_rooms SET rm_status = 1 WHERE rm_id = " . $prev_records['rt_room_id'];
            $qry->exec($sql);
        }


        $sql = "UPDATE tbl_rooms SET rm_status = 0 WHERE rm_id = " . $post['room'];
        $qry->exec($sql);

        $rs = $qry->resultMsg;
        $rs['params'] = '?id=' . $post['room'] .'&rt_id='.$rt_id;

        $app->json_encode($rs);
}
