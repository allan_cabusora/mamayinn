<?php

$rm_id = (isset($_GET['id'])) ? $_GET['id'] : 0;
$rt_id = (isset($_GET['rt_id'])) ? $_GET['rt_id'] : -1;

unset($_SESSION['charges_' . $rm_id]);
unset($_SESSION['index_charges_' . $rm_id]);

$_contents->setVariable('rt_id', $rt_id);
$_contents->setVariable('room_id', $rm_id);


$sql = "SELECT * FROM tbl_rooms
        INNER JOIN tbl_room_types ON rm_type = rtp_id
        WHERE rm_id = " . $rm_id;

$rt = $qry->getRecords($sql);
foreach($rt['data'] as $row){
    $_contents->setVariable($row);
    $rm_type = $row['rm_type'];
}

$sql = "SELECT * FROM tbl_rooms ORDER BY rm_number ASC";

$rooms = $qry->getRecords($sql);
foreach($rooms['data'] as $row){
    $_contents->setCurrentBlock('rooms');
    $_contents->setVariable($row);
    if($row['rm_status'] != 1){
        $_contents->setVariable('rm_disabled', 'disabled');
    }

    if($row['rm_id'] == $rm_id) $_contents->setVariable('rm_selected', 'SELECTED');

    $_contents->parseCurrentBlock();
}

$rm_day = date('N');

$sql = "SELECT * FROM tbl_room_rates
        INNER JOIN tbl_room_types ON rr_room_type = rtp_id
        WHERE rr_room_type = {$rm_type} AND rr_room_day = {$rm_day}
        ORDER BY rr_hours ASC";

$room_rates = $qry->getRecords($sql);
foreach($room_rates['data'] as $row){
    $_contents->setCurrentBlock('room_rates');
    $_contents->setVariable($row);
    //if($row['rm_id'] == $rm_type_id) $_contents->setVariable('rm_selected', 'SELECTED');

    $_contents->parseCurrentBlock();
}


$sql = "SELECT * FROM tbl_vehicles";
$room_rates = $qry->getRecords($sql);
foreach($room_rates['data'] as $row){
    $_contents->setCurrentBlock('vehicles');
    $_contents->setVariable($row);
    //if($row['rm_id'] == $rm_type_id) $_contents->setVariable('rm_selected', 'SELECTED');

    $_contents->parseCurrentBlock();
}


$_contents->setCurrentBlock('total');
$_contents->setVariable('TOTAL', '1000.00');
$_contents->parseCurrentBlock();