<?php

$id = $_GET['id'];
$rt_id = $_GET['rt_id'];
$rm_id = $_GET['rm_id'];
$_contents->setVariable('id', $id);
$_contents->setVariable('rt_id', $rt_id);
$_contents->setVariable('rm_id', $rm_id);

$purchase_charges = array();

if($rt_id > 0){
    $sql = 'SELECT * FROM tbl_room_transaction_charges
            INNER JOIN tbl_room_transactions ON rc_room_transaction = rt_id
            INNER JOIN tbl_extra_charges ON rc_ec_id = ec_id
            WHERE rc_id='.$id;
    $records = $qry->getRecords($sql);
    $cat_id = 0;

    foreach($records['data'] as $row){
        $_contents->setVariable($row);
        $cat_id = $row['ec_cat_id'];
    }


    $sql = 'SELECT * FROM tbl_room_transaction_charges WHERE rc_room_transaction='.$rt_id;
    $records2 = $qry->getRecords($sql);

    foreach($records2['data'] as $record){
        $purchase_charges[] = $record['rc_ec_id'];
    }
}else{

    $rc_id = $_GET['rc_id'];

    if(isset($_SESSION['charges_' . $rm_id])){
        $charges = $_SESSION['charges_' . $rm_id];
        foreach($charges as $charge){
            if($id == $charge['rc_id']){
                $_contents->setVariable($charge);
                $cat_id = $charge['cat_id'];
            }
            $purchase_charges[] = $charge['ec_id'];
        }
    }
}

$_contents->setVariable('readonly', 'readonly');


//$sql = 'SELECT * FROM tbl_charges WHERE ec_id NOT IN (' . implode(',', $purchase_charges) . ') OR ec_id = ' . $rc_ec_id . ' ORDER BY ec_description ASC';
$sql = 'SELECT * FROM tbl_category_charges
          INNER JOIN tbl_extra_charges ON ec_cat_id = cat_id
          GROUP BY cat_id
          ORDER BY cat_description ASC';
$charges = $qry->getRecords($sql);

foreach($charges['data'] as $row){
    $_contents->setCurrentBlock('category');
    $_contents->setVariable($row);
    if($row['cat_id'] == $cat_id) $_contents->setVariable('cat_selected', 'SELECTED');
    $_contents->parseCurrentBlock();
}