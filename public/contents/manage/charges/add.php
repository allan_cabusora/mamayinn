<?php

$id = $_GET['id'];
$rt_id = (isset($_GET['rt_id'])) ? $_GET['rt_id'] : 0;
$rm_id = (isset($_GET['rm_id'])) ? $_GET['rm_id'] : 0;

$_contents->setVariable('rt_id', $rt_id);
$_contents->setVariable('rm_id', $rm_id);

$purchase_charges = array();

if($rt_id > 0){

    $sql = 'SELECT * FROM tbl_room_transaction_charges WHERE rc_room_transaction='.$rt_id;
    $records2 = $qry->getRecords($sql);

    foreach($records2['data'] as $record){
        $purchase_charges[] = $record['rc_ec_id'];
    }

}else {
    if (isset($_SESSION['charges_' . $rm_id])) {
        $charges = $_SESSION['charges_' . $rm_id];
        foreach ($charges as $charge) {
            $purchase_charges[] = $charge['ec_id'];
        }
    }
}

if(count($purchase_charges) == 0)
    $purchase_charges[] = 0;

//$sql = 'SELECT * FROM tbl_category_charges WHERE ec_id NOT IN (' . implode(',', $purchase_charges) . ') ORDER BY ec_description ASC';
$sql = 'SELECT * FROM tbl_category_charges
          INNER JOIN tbl_extra_charges ON ec_cat_id = cat_id
          GROUP BY cat_id
          ORDER BY cat_description ASC';
$charges = $qry->getRecords($sql);


foreach($charges['data'] as $row){
    $_contents->setCurrentBlock('category');
    $_contents->setVariable($row);
    $_contents->parseCurrentBlock();
}
