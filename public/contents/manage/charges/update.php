<?php
$post = $_POST;
$action = $post['action'];



if($action == 'add'){

    if($post['rt_id'] > 0){

        $sql = "SELECT * FROM tbl_extra_charges WHERE ec_id = " . $post['ec_id'];
        $data = $qry->getRecord($sql);

        $sql = "INSERT into tbl_room_transaction_charges SET
                  rc_ec_id = {$post['ec_id']},
                  rc_room_transaction = {$post['rt_id']},
                  rc_description = '{$data['ec_name']}',
                  rc_qty = {$post['qty']},
                  rc_rate = {$post['charge_rate']}" ;
        $qry->exec($sql);

        $app->json_encode($qry->resultMsg);
    }else{
        if(!isset($_SESSION['charges_' . $post['rm_id']])){
            $_SESSION['charges_' . $post['rm_id']] = array();
        }

        if(!isset($_SESSION['index_charges_' . $post['rm_id']])){
            $_SESSION['index_charges_' . $post['rm_id']] = 0;
        }



        $sql = "SELECT * FROM tbl_extra_charges WHERE ec_id = " . $post['ec_id'];
        $data = $qry->getRecord($sql);

        $charges = $_SESSION['charges_' . $post['rm_id']];
        $index = $_SESSION['index_charges_' . $post['rm_id']];
        $data = array(
            'rc_room_transaction' => -1,
            'ec_id' => $post['ec_id'],
            'cat_id' => $post['cat_id'],
            'ec_description' => $data['ec_name'],
            'rc_rate' => $data['ec_price'],
            'rc_qty' => $post['qty'],
            'rc_id' => $index,
            'sub_total' => number_format($post['qty'] * $data['ec_price'], 2)
        );

        array_push($charges, $data);

        $_SESSION['charges_' . $post['rm_id']] = $charges;
        $_SESSION['index_charges_' . $post['rm_id']]++;


        $app->json_encode(array('status' => 'success', 'action' => 'temp', 'lastid' => -1));
    }


}else{

    if($post['rt_id'] > 0){

        if($post['qty'] > 0){

            $sql = "SELECT * FROM tbl_extra_charges WHERE ec_id = " . $post['ec_id'];
            $data = $qry->getRecord($sql);

            $sql = "UPDATE tbl_room_transaction_charges SET
                      rc_ec_id = {$post['ec_id']},
                      rc_room_transaction = {$post['rt_id']},
                      rc_description = '{$data['ec_name']}',
                      rc_qty = {$post['qty']} ,
                      rc_rate = {$post['charge_rate']}
                      WHERE rc_id = " . $post['id'] ;
            $qry->exec($sql);

            $app->json_encode($qry->resultMsg);
        }else{
            $sql = "DELETE from tbl_room_transaction_charges WHERE rc_id = " . $post['id'] ;
            $qry->exec($sql);

            $r = array(
                'status' => 'success',
                'action' => 'delete',
                'lastid' => -1
            );
            $app->json_encode($r);
        }


    }else{

        $charges = $_SESSION['charges_' . $post['rm_id']];
//        print_r($post);
        if($post['qty'] > 0){
            $new_charges = array();

            foreach($charges as $charge){
                if($post['id'] == $charge['rc_id']){

                    $sql = "SELECT * FROM tbl_extra_charges WHERE ec_id = " . $post['ec_id'];
                    $ec_data = $qry->getRecord($sql);

                    $data = array(
                        'rc_room_transaction' => -1,
                        'ec_id' => $post['ec_id'],
                        'cat_id' => $post['cat_id'],
                        'ec_description' => $ec_data['ec_name'],
                        'rc_rate' => $ec_data['ec_price'],
                        'rc_qty' => $post['qty'],
                        'rc_id' => $charge['rc_id'],
                        'sub_total' => number_format($post['qty'] * $ec_data['ec_price'], 2)
                    );

                    $new_charges[] = $data;

                }else{
                    $new_charges[] = $charge;
                }
            }

//            print_r($new_charges);
            $_SESSION['charges_' . $post['rm_id']] = $new_charges;

        }else{
            $to_delete = 0;
            foreach($charges as $charge){
                if($post['id'] == $charge['rc_id']){
                    break;
                }
                $to_delete++;
            }

            array_splice($charges, $to_delete, 1);

            $_SESSION['charges_' . $post['rm_id']] = $charges;

        }

        $app->json_encode(array('status' => 'success', 'action' => 'temp', 'lastid' => -1));
    }
}


