<?php

$type = $_GET['type'];

$sql = 'SELECT
        *,
        GROUP_CONCAT(rr_room_day ORDER BY rr_room_day ASC) as room_day,
        GROUP_CONCAT(rr_rate ORDER BY rr_room_day ASC) as room_rate,
        GROUP_CONCAT(rr_excess_rate ORDER BY rr_room_day ASC) as excess_rate
        FROM
        tbl_room_rates
        LEFT JOIN tbl_room_types ON rr_room_type = rtp_id
        GROUP BY rr_room_type,rr_hours
        ORDER BY rtp_name DESC';
$paging = ['currPage'=>$_GET['page'], 'recPerPage'=>10];             // optional parameter to set the returns in pages
$records = $qry->getRecords($sql, $paging);                              // returns associative array [info, data]


foreach($records['data'] as &$data){
    $days = explode(",", $data['room_day']);
    $rate = explode(",", $data['room_rate']);
    $excess_rate = explode(",", $data['excess_rate']);

    foreach($days as $key=>$day){
        $data['rr_day' . $day] = $rate[$key];
        $data['rr_excess' . $day] = $excess_rate[$key];
    }
}

$app->json_encode($records);


