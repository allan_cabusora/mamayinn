<?php

$id = (isset($_GET['id'])) ? $_GET['id'] : 0;

$sql = "SELECT * FROM tbl_room_rates WHERE rr_id = {$id}";
$rr = $qry->getRecord($sql);

$sql = "SELECT *,
        GROUP_CONCAT(rr_id ORDER BY rr_id ASC) as room_id,
        GROUP_CONCAT(rr_room_day ORDER BY rr_room_day ASC) as room_day,
        GROUP_CONCAT(rr_rate ORDER BY rr_room_day ASC) as room_rate,
        GROUP_CONCAT(rr_excess_rate ORDER BY rr_room_day ASC) as excess_rate
        FROM
        tbl_room_rates
        LEFT JOIN tbl_room_types ON rr_room_type = rtp_id
        WHERE rr_room_type={$rr['rr_room_type']} AND rr_hours={$rr['rr_hours']}
        GROUP BY rr_room_type,rr_hours
        ORDER BY rtp_name DESC";
$record = $qry->getRecord($sql);

    $ids = explode(",", $record['room_id']);
    $days = explode(",", $record['room_day']);
    $rate = explode(",", $record['room_rate']);
    $excess_rate = explode(",", $record['excess_rate']);

    foreach($days as $key=>$day){
        $record['rr_id_' . $day] = $ids[$key];
        $record['rr_day' . $day] = $rate[$key];
        $record['rr_excess' . $day] = $excess_rate[$key];
    }

$_contents->setVariable($record);


$sql = "SELECT * FROM tbl_room_types ORDER BY rtp_name ASC";
$rooms = $qry->getRecords($sql);
foreach($rooms['data'] as $row){
    $_contents->setCurrentBlock('room_types');
    $_contents->setVariable($row);
    if($row['rtp_id'] == $record['rr_room_type']) $_contents->setVariable('rtp_selected', 'SELECTED');
    $_contents->parseCurrentBlock();
}

