<?php

$type = $_GET['type'];

$sql = 'SELECT
        *,
        IF(rm_status = 3, "<span class=\"label label-warning\">Dirty</span>", ( IF(rm_status = 2, "<span class=\"label label-danger\">Out of Order</span>", "<span class=\"label label-success\">Available</span>") )) as status
        FROM
        tbl_rooms
        LEFT JOIN tbl_room_types ON rm_type = rtp_id
        ORDER BY rm_number ASC';
$paging = ['currPage'=>$_GET['page'], 'recPerPage'=>20];             // optional parameter to set the returns in pages
$records = $qry->getRecords($sql, $paging);                              // returns associative array [info, data]
$app->json_encode($records);


