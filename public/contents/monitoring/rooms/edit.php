<?php

$id = $_GET['id'];
$sql = 'SELECT * FROM tbl_rooms WHERE rm_id='.$id;
$record = $qry->getRecord($sql);

$_contents->setVariable($record);


$sql = "SELECT * FROM tbl_room_types
        ORDER BY rtp_name ASC";
$room_rates = $qry->getRecords($sql);
foreach($room_rates['data'] as $row){
    $_contents->setCurrentBlock('room_types');
    $_contents->setVariable($row);
    if($row['rtp_id'] == $record['rm_type']) $_contents->setVariable('rtp_selected', 'SELECTED');
    $_contents->parseCurrentBlock();
}


$status = array(
    array(
        't_id' => 0,
        't_desc' => 'Enable'
    ),array(
        't_id' => 2,
        't_desc' => 'Out of Order'
    ),array(
        't_id' => 3,
        't_desc' => 'Dirty'
    )
);

foreach($status as $row){
    $_contents->setCurrentBlock('room_status');
    $_contents->setVariable($row);
    if($row['t_id'] == $record['rm_status']) $_contents->setVariable('t_selected', 'SELECTED');
    $_contents->parseCurrentBlock();
}