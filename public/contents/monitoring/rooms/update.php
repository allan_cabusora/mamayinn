<?php
$post = $_POST;                                            // get $_POST variables
$type = $post['type'];

unset($post['type']);
$post['type'] = $post['types'];

$tablePrefix = 'rm_';                                    // set table prefix for books

$addValid = ['REQUIRED' => 'number'];

if($post['status'] == 0){
    unset($post['status']);
}

if ($qry->generateSQL($post, 'tbl_'.$type, $tablePrefix, $post['action'], $tablePrefix.'id='.$post['id'], $addValid)) {
    $qry->execSQL();
}
$app->json_encode($qry->resultMsg);
