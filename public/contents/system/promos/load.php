<?php

$type = $_GET['type'];

$sql = 'SELECT
        *,
        CONCAT((prm_discount_percentage*100), "%") as pdp,
        IF(prm_status=1,"<span class=\"label label-success\">Active</span>","<span class=\"label label-danger\">In-Active</span>") as status
        FROM
        tbl_promos
        LEFT JOIN tbl_room_types ON rtp_id = prm_rm_type
        ORDER BY prm_date_created ASC';

$paging = ['currPage'=>$_GET['page'], 'recPerPage'=>10];             // optional parameter to set the returns in pages
$records = $qry->getRecords($sql, $paging);                              // returns associative array [info, data]
$app->json_encode($records);


