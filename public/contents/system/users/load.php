<?php

$type = $_GET['type'];

$sql = 'SELECT
        u_id,
         CONCAT(u_firstname, " ", u_middlename, " ", u_lastname ) as name,
         u_username,
         IF(u_gender = 0, "Male", "Female") as gender,
         u_birthdate
        FROM
        tbl_users
        ORDER BY u_lastname ASC';
$paging = ['currPage'=>$_GET['page'], 'recPerPage'=>10];             // optional parameter to set the returns in pages
$records = $qry->getRecords($sql, $paging);                              // returns associative array [info, data]
$app->json_encode($records);


