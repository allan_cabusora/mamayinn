<?php

$genders = array(0 => "Male", 1 => "Female");
foreach($genders as $key => $gender){
    $_contents->setCurrentBlock('gender');
    $_contents->setVariable('gender_id', $key);
    $_contents->setVariable('gender_name', $gender);
    $_contents->parseCurrentBlock();
}

$sql = 'SELECT * FROM tbl_usertypes ORDER BY ut_name ASC';
$genres = $qry->getRecords($sql);
foreach($genres['data'] as $row){
    $_contents->setCurrentBlock('usertypes');
    $_contents->setVariable($row);
    $_contents->parseCurrentBlock();
}