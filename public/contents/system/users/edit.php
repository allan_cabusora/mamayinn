<?php

$id = $_GET['id'];
$sql = 'SELECT * FROM tbl_users WHERE u_id='.$id;
$record = $qry->getRecord($sql);
$_contents->setVariable($record);



$genders = array(0 => "Male", 1 => "Female");
foreach($genders as $key => $gender){
    $_contents->setCurrentBlock('gender');
    $_contents->setVariable('gender_id', $key);
    $_contents->setVariable('gender_name', $gender);
    if ($record['u_gender'] == $key) $_contents->setVariable('gender_selected', 'SELECTED');
    $_contents->parseCurrentBlock();
}

$sql = 'SELECT * FROM tbl_usertypes ORDER BY ut_name ASC';
$genres = $qry->getRecords($sql);
foreach($genres['data'] as $row){
    $_contents->setCurrentBlock('usertypes');
    $_contents->setVariable($row);
    if ($record['u_usertype'] == $row['ut_id']) $_contents->setVariable('ut_selected', 'SELECTED');
    $_contents->parseCurrentBlock();
}
