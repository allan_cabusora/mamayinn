<?php
require_once(INCLUDEPATH . "fpdf17/fpdf.php");
// Send Headers

$where = "WHERE rt_checked_out_by = " . $_SESSION['u_id'];
$sort = "rt_checked_out";
$startDate = date('Y-m-d') . ' 00:00:00';
$endDate = date('Y-m-d') . ' 23:59:59';

if(isset($_GET['date_range'])){
    /*$range = explode(" - ", $_GET['date_range']);
    $where .= " WHERE rt_checked_out >= '" . date('Y-m-d', strtotime($range[0]) ) . " 00:00:00' AND rt_checked_out <= '" . date('Y-m-d', strtotime($range[1]) ) . " 23:59:59'";
    $filename = "Summary_Reports-" . $range[0] . '_to_' . $range[1] . '.xlsx';*/

    $date = $_GET['date_range'];
    $where .= " AND rt_checked_out >= '" . date('Y-m-d', strtotime($date) ) . " 08:00:00' AND rt_checked_out <= '" . date('Y-m-d', strtotime($date  . '+1 day') ) . " 08:00:00'";
    $filename = "Summary_Reports-" . date('Y-m-d', strtotime($date) ) . '.xlsx';

    $startDate = date('Y-m-d', strtotime($date) ) . " 08:00 AM";
    $endDate = date('Y-m-d', strtotime($date  . '+1 day') ) . " 08:00 AM";
//    $where = " WHERE DATE(rt_date_created) BETWEEN '2015-08-01' AND '2015-08-30'";

    $shifts = array(
                    array(
                        'start' => date('Y-m-d', strtotime($date) ) . " 08:00:00",
                        'end'   => date('Y-m-d', strtotime($date) ) . " 15:59:59"
                    ),
                    array(
                        'start' => date('Y-m-d', strtotime($date) ) . " 16:00:00",
                        'end'   => date('Y-m-d', strtotime($date) ) . " 23:59:59"
                    ),
                    array(
                        'start' => date('Y-m-d', strtotime($date . '+1 day') ) . " 00:00:00",
                        'end'   => date('Y-m-d', strtotime($date . '+1 day') ) . " 07:59:59"
                    )
                );
}else{
    $where .= " AND rt_checked_out >= '" . date('Y-m-d') . " 00:00:00' AND rt_checked_out <= '" .  date('Y-m-d', strtotime( date('Y-m-d') . '+1 day')) . " 23:59:59'";
    $filename = "Summary_Reports-" . date('Y-m-d') . '.xlsx';

    $shifts = array(
        array(
            'start' => date('Y-m-d', strtotime( date('Y-m-d')) ) . " 08:00:00",
            'end'   => date('Y-m-d', strtotime( date('Y-m-d')) ) . " 15:59:59"
        ),
        array(
            'start' => date('Y-m-d', strtotime( date('Y-m-d')) ) . " 16:00:00",
            'end'   => date('Y-m-d', strtotime( date('Y-m-d')) ) . " 23:59:59"
        ),
        array(
            'start' => date('Y-m-d', strtotime( date('Y-m-d') . '+1 day') ) . " 00:00:00",
            'end'   => date('Y-m-d', strtotime( date('Y-m-d') . '+1 day') ) . " 07:59:59"
        )
    );
}


function getShift($date, $shifts = array()){
    foreach($shifts as $key => $shift){
        if(strtotime($date) >= strtotime($shift['start']) && strtotime($date) <= strtotime($shift['end']) ){
            return $key+1;
        }
    }

}

$sql = 'SELECT
            rt_id,
            rt_room_id,
            rt_checked_in,
            rt_checked_out,
            rt_customer_name,
            rt_extended_hours,
            rt_hourly_rate,
            DATE_FORMAT(rt_checked_in, "%Y-%m-%d %h:%i %p") as check_in,
            DATE_FORMAT(rt_checked_out, "%Y-%m-%d %h:%i %p") as check_out,
            rt_hours,
            rt_rr_rate,
            rc_qty,
            rc_description,
            rc_rate,
            rt_promo_code,
            rt_promo_rate
        FROM
        tbl_room_transactions
        LEFT JOIN tbl_room_transaction_charges ON rc_room_transaction = rt_id
        LEFT JOIN tbl_users ON u_id = rt_user_id
        ' . $where . '
        ORDER BY rt_checked_out ASC';
//echo $sql; exit;
$paging = ['currPage'=>$_GET['page'], 'recPerPage'=>10];             // optional parameter to set the returns in pages
$records = $qry->getRecords($sql, $paging);                              // returns associative array [info, data]

$ndata = array();
$rt_id = 0;
$shift = array();

foreach($records['data'] as $key=>$value){
    $temp = array();

    if($value['rt_id'] != $rt_id){
        $temp['rt_id'] = $value['rt_id'];
        $temp['rm_number'] = $value['rt_room_id'];
        $temp['check_in'] = $value['check_in'];
        $temp['check_out'] = $value['check_out'];
        $temp['customer'] = $value['rt_customer_name'];

        if($value['rt_room_id'] == 999){

        }else{
            if($value['rt_hours'] == 0){
                $temp['promo'] = "";
                $temp['qty'] = $value['rt_extended_hours'];
                $temp['desc'] = $value['rt_extended_hours'] . " HRS - Open Time";
                $temp['amount'] = number_format( $value['rt_hourly_rate'] * $value['rt_extended_hours'], 2);
                $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;
            }else{
                //$temp['qty'] = $value['rt_hours'];
                $temp['qty'] = 1;
                $temp['desc'] = $value['rt_hours'] . " HRS - Regular";

                $disc = $value['rt_rr_rate'] * $value['rt_promo_rate'];

                $temp['promo'] = $value['rt_promo_code'] ? $value['rt_promo_code'] : "";
                $temp['amount'] = number_format($value['rt_rr_rate'] - $disc, 2 );
                $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;

                if($value['rt_extended_hours'] > 0){
                    $temp['rt_id'] = "";
                    $temp['rm_number'] = "";
                    $temp['check_in'] = "";
                    $temp['check_out'] = "";
                    $temp['customer'] = "";
                    $temp['promo'] = "";

                    $temp['qty'] = $value['rt_extended_hours'];
                    $temp['desc'] = "Extended Hours";
                    $temp['amount'] = number_format($value['rt_hourly_rate'] * $value['rt_extended_hours'], 2);
                    $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;
                }
            }
        }

        if($value['rc_qty'] > 0){
            if($value['rt_room_id'] != 999){
                $temp['rt_id'] = "";
                $temp['rm_number'] = "";
                $temp['check_in'] = "";
                $temp['check_out'] = "";
                $temp['customer'] = "";
                $temp['promo'] = "";
            }

            $temp['qty'] = $value['rc_qty'];
            $temp['desc'] = $value['rc_description'];
            $temp['amount'] = number_format($value['rc_qty'] * $value['rc_rate'], 2 );
            $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;
        }

        $rt_id = $value['rt_id'];
    }else{
        $temp['rm_number'] = "";
        $temp['check_in'] = "";
        $temp['check_out'] = "";

        $temp['qty'] = $value['rc_qty'];
        $temp['desc'] = $value['rc_description'];
        $temp['amount'] = number_format( $value['rc_rate'], 2 );
        $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;
    }
}


class PDF extends FPDF
{
// Page header

    var $date_start = "";
    var $date_end = "";
    var $header = "";
    var $width = "";
    var $align = "";
    var $shifts = array();

    function Header()
    {
        $this->SetFont('Arial','B',11);
        $this->Cell(0,1,'Mamay Road, Brgy Angliongto, Davao City',0,1,'C');
        $this->Cell(0,8,'Tel. No. (082) 305-5936',0,1,'C');

        // Logo
        $this->Image('images/Mamaylogo.png',10,6,30);
        $this->SetFont('Arial','B',10);
        $this->Cell(0,6, 'From: ' .$this->date_start . ' To: ' . $this->date_end,0,1,'C');
        // Header
        $this->SetFont('Arial','B',8);
        foreach($this->header as $key=>$col)
            $this->Cell($this->width[$key],7,$col,'TB', 0, $this->align[$key]);
        $this->Ln();
    }

// Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,$this->PageNo(),0,0,'C');
    }


// Better table
    function BasicTable($header, $width, $data, $align, $tot)
    {
        // Data
        $grand_total = 0;
        foreach($data as $key=>$shift){
            $this->SetFont('Arial','B',10);
            $this->Cell($width[0],6,$this->ordinal($key). " Shift",0,0, $align[0]);
            $this->Cell($width[1],6,"",0,0, $align[1]);
            $this->Cell($width[2],6,"",0,0, $align[2]);
            $this->Cell($width[3],6,"",0,0, $align[3]);
            $this->Cell($width[4],6,"",0,0, $align[4]);
            $this->Cell($width[5],6,"",0,0, $align[5]);
            $this->Cell($width[6],6,"",0,0, $align[6]);
            $this->Cell($width[7],6,"",0,0, $align[7]);
            $this->Cell($width[8],6,"",0,0, $align[8]);
            $this->Ln();

            $sub_grand_total = 0;
            foreach($shift as $key=>$row)
            {
                $this->SetFont('Arial','',8);
                $this->Cell($width[0],6,($row['rt_id'] != "") ? str_pad($row['rt_id'], 6, "0", STR_PAD_LEFT) : "",0,0, $align[0]);
                $this->Cell($width[1],6,$row['rm_number'],0,0, $align[1]);
                $this->Cell($width[2],6,$row['check_in'],0,0, $align[2]);
                $this->Cell($width[3],6,$row['check_out'],0,0, $align[3]);
                $this->Cell($width[4],6,$row['customer'],0,0, $align[4]);
                $this->Cell($width[5],6,$row['promo'],0,0, $align[5]);
                $this->Cell($width[6],6,$row['qty'],0,0, $align[6]);
                $this->Cell($width[7],6,$row['desc'],0,0, $align[7]);
                $this->Cell($width[8],6,$row['amount'],0, 0, $align[8]);
                $grand_total += str_replace(",","",$row['amount']);
                $sub_grand_total += str_replace(",","",$row['amount']);
                $this->Ln();
            }

            $this->SetFont('Arial','',8);
            $this->Cell($width[0],6,"",0,0, $align[0]);
            $this->Cell($width[1],6,"",0,0, $align[1]);
            $this->Cell($width[2],6,"",0,0, $align[2]);
            $this->Cell($width[3],6,"",0,0, $align[3]);
            $this->Cell($width[4],6,"",0,0, $align[4]);
            $this->Cell($width[5],6,"",0,0, $align[5]);
            $this->Cell($width[6],6,"",0,0, $align[6]);
            $this->Cell($width[7],6,"",0,0, $align[7]);
            $this->Cell($width[8],6,number_format($sub_grand_total, 2),'T', 0, $align[8]);
            $this->Ln();
        }

        $this->SetFont('Arial','B',8);
        $this->Cell($width[0],6,"",0,0, $align[0]);
        $this->Cell($width[1],6,"",0,0, $align[1]);
        $this->Cell($width[2],6,"",0,0, $align[2]);
        $this->Cell($width[3],6,"",0,0, $align[3]);
        $this->Cell($width[4],6,"",0,0, $align[4]);
        $this->Cell($width[5],6,"",0,0, $align[5]);
        $this->Cell($width[6],6,"",0,0, $align[6]);
        $this->Cell($width[7],6,"",0,0, $align[7]);
        $this->Cell($width[8],6,number_format($grand_total, 2),'T', 0, $align[8]);
        $this->Ln();


        $this->Ln();
        $sales = $this->getSalesByShift($data);
        $this->Ln();

        $this->Cell(30,6,"Cashier In-Charge: ",0,0, 'L');
        $this->SetFont('Arial','',8);
        $this->Cell(30,5,$_SESSION['name'],'B',0, 'R');


        $this->Ln();

        $this->SetFont('Arial','B',8);
        $this->Cell(30,6,"Total Customers: ",0,0, 'L');

        $this->SetFont('Arial','',8);
        $custs = $this->getCustomerByShift($data);
        $tot_cust = 0;
        foreach($custs as $key=>$cust){
            $tot_cust+=$cust;
        }
        $this->Cell(30,5,$tot_cust,'B',0, 'R');

        $this->Ln();


        $this->SetFont('Arial','B',8);
        $this->Cell(30,6,"Total Sales: ",0,0, 'L');
        $tot_sale = 0;
        foreach($sales as $key=>$sale){
            $tot_sale+=$sale;
        }
        $this->SetFont('Arial','',8);
        $this->Cell(30,5,number_format($tot_sale, 2),'B',0, 'R');


    }


    function getCustomerByShift($data){

        $tot_customer = array();
        foreach($this->shifts as $key=>$shift){
            if(isset($data[$key+1])){
                $cnt = 0;
                foreach($data[$key+1] as $row){
                    if($row['rm_number'] != "")
                        $cnt++;
                }
                $tot_customer[] = $cnt;
            }else{
                $tot_customer[] = 0;
            }

        }

        return $tot_customer;
    }

    function ordinal($number) {
        $ends = array('th','st','nd','rd','th','th','th','th','th','th');
        if ((($number % 100) >= 11) && (($number%100) <= 13))
            return $number. 'th';
        else
            return $number. $ends[$number % 10];
    }

    function getSalesByShift($data){

        $tot_sales = array();
        foreach($this->shifts as $key=>$shift){
            if(isset($data[$key+1])){
                $cnt = 0;
                foreach($data[$key+1] as $row){
                    $cnt += str_replace(",","",$row['amount']);
                }
                $tot_sales[] = $cnt;
            }else{
                $tot_sales[] = 0;
            }

        }

        return $tot_sales;
    }
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();


$header = array('Ref #', 'Room', 'Check-In', 'Check-Out', 'Customer', 'VIP #', 'Qty', 'Description', 'Amount');
$width = array(12, 10, 30, 30, 35, 10, 15, 35, 20);
$align = array('L','L', 'L', 'L', 'L', 'L', 'R', 'L', 'R');
// Data loading

$pdf->date_start = date('F j, Y, g:i A', strtotime($startDate) );
$pdf->date_end = date('F j, Y, g:i A', strtotime($endDate) );
$pdf->header = $header;
$pdf->align = $align;
$pdf->width = $width;
$pdf->shifts = $shifts;

$pdf->AddPage('P', 'Letter');
$pdf->SetFont('Times','',12);
$pdf->BasicTable($header,$width,$ndata, $align, $tot);

//$pdf->Output('CollectionReport.pdf', 'D');
$pdf->Output();