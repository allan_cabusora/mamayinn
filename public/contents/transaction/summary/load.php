<?php

//print_r($_GET);

$where = "";
$sort = "rpt_checked_in";

if(isset($_GET['sort'])){
    if($_GET['sort'] == 'room'){
        $sort = "rm_number";
    }
}
$where = "WHERE rpt_user_id = " . $_SESSION['u_id'];
if(isset($_GET['date_range'])){
    $range = explode(" - ", $_GET['date_range']);
    $where .= " AND rpt_checked_in >= '" . date('Y-m-d', strtotime($range[0]) ) . " 00:00:00' AND rpt_checked_in <= '" . date('Y-m-d', strtotime($range[1]) ) . " 23:59:59'";
    $filename = "Summary_Reports-" . $range[0] . '_to_' . $range[1] . '.xlsx';
//    $where = " WHERE DATE(rt_date_created) BETWEEN '2015-08-01' AND '2015-08-30'";
}else{
    $where .= " AND rpt_checked_in >= '" . date('Y-m-d') . " 00:00:00' AND rpt_checked_in <= '" . date('Y-m-d') . " 23:59:59'";
    $filename = "Summary_Reports-" . date('Y-m-d') . '.xlsx';
}


$sql = 'SELECT
            rpt_rm_number as rm_number,
            rpt_rm_type as rtp_name,
            DATE_FORMAT(rpt_checked_in, "%b %d, %Y %h:%i %p") as check_in,
            DATE_FORMAT(rpt_checked_out, "%b %d, %Y %h:%i %p") as check_out,
            rpt_hours as hours,
            rpt_extra_charge as extra_charges,
            rpt_room_charge as room_charge,
            rpt_extra_charge + rpt_room_charge as total_charge
        FROM
        tbl_reports
        ' . $where . '
        ORDER BY ' . $sort . ' ASC';
//echo $sql;
$paging = ['currPage'=>$_GET['page'], 'recPerPage'=>10];             // optional parameter to set the returns in pages
$records = $qry->getRecords($sql, $paging);                              // returns associative array [info, data]
$app->json_encode($records);


