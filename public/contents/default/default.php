<?php

$_contents->setVariable('clock', date('Y-m-d H:m:s'));

$sql = 'SELECT * FROM tbl_rooms
        LEFT JOIN tbl_room_transactions ON rt_status = 0 AND rt_room_id = rm_id
        LEFT JOIN tbl_room_transaction_charges ON rc_room_transaction = rt_id AND rc_ec_id = 1
        LEFT JOIN tbl_room_types ON rm_type = rtp_id
        WHERE rm_number != 999
        GROUP BY rm_id
        ORDER BY rm_status ASC, rt_checked_in ASC, rm_number ASC';
$rooms = $qry->getRecords($sql);
foreach($rooms['data'] as $row){

    $_contents->setCurrentBlock('rooms');
    $_contents->setVariable('rm_number', sprintf("%03d", $row['rm_number']));

    $extra_time = ($row['rt_extended_hours']) ? $row['rt_extended_hours'] : 0;
    $_contents->setVariable('rt_hours', $row['rt_hours'] + $extra_time);

    $_contents->setVariable('rm_class',  str_replace(" ", "_", strtolower($row['rtp_name'])));

    $room_stat = 'available';
    $new_date = date('Y-m-d H:i:s', strtotime('+' . $row['rt_hours']+$extra_time . ' hours', strtotime($row['rt_checked_in'])));
    if($row['rt_id'] && $row['rt_status'] == 0 && $row['rm_status'] == 0){
        $room_stat = 'occupied';
//        $_contents->setVariable('rm_time', $new_date);
        $_contents->setVariable('rm_time', $row['rt_checked_in']);

        $_contents->setVariable('in', '<span class="rm_in">' . date('h:i A', strtotime($row['rt_checked_in']) ) . '</span>' );

        $out = ($row['rt_hours']) ? date('h:i A', strtotime($new_date) ) : "--:--";
        if(!$row['rt_hours']){
            $room_stat = 'occupied-open';
        }

        $_contents->setVariable('out', '<span class="rm_out">' . $out . '</span>' );

        $now = new DateTime($row['rt_checked_in']);
        $future_date = new DateTime($new_date);
        $interval = $future_date->diff($now);

        $_contents->setVariable('out_minutes',  $row['rt_hours'] );

        if(date('Y-m-d H:i:s', strtotime('+10 minutes', time())) > $new_date && $row['rt_hours']){
            $room_stat = "end";
        }
        $_contents->setVariable('action', 'edit');
        $_contents->setVariable('available', 'available');
    }else{
        $_contents->setVariable('action', 'add');
        $_contents->setVariable('in', $row['rtp_name']);
        $_contents->setVariable('out', '<i class="fa fa-arrow-circle-right"></i>');

        if($row['rm_status'] == 3){
            $room_stat = "dirty";
            $_contents->setVariable('available', 'dirty');
        }else if($row['rm_status'] == 2){
            $room_stat = "out-of-order";
            $_contents->setVariable('available', 'out-of-order');
        }else{
            $_contents->setVariable('available', 'available');
        }
    }

    $_contents->setVariable('rt_id', ($row['rt_id'] && $row['rt_status'] == 0 && $row['rm_status'] == 0) ? $row['rt_id'] : -1);
    $_contents->setVariable('rm_id', $row['rm_id']);

    $_contents->setVariable('color_status', $room_stat);
    $_contents->setVariable('rm_status', $room_stat);

    $_contents->parseCurrentBlock();
}
