<?php

$route = $app::$route;
$folderURI = $route['uri'];

if (strlen($folderURI)==0) {
	$nav = 'default';
	$folderURI = '../../';
} else {
	$nav = $folderURI;
}
//$_theme->setVariable('menu', "TAE");

if($folderURI != "auth/login" && !isset($_SESSION['u_id'])){
	header('Location: /auth/login');
}

if($folderURI == "auth/login" && isset($_SESSION['u_id'])){
	header('Location: /');
}

$readmeFile = str_replace('//','/', 'contents/'.$folderURI.'/README.md');

$readme = '';
$parsedown = new Parsedown;

if (file_exists($readmeFile)) {
	$readmeRender = new Lazy\Render('contents/'.$folderURI, 'README.md');
	$readmeRender->setVariable($route);
	$readme = $parsedown->text($readmeRender->get());
} else {
	$readmeFile = 'None';
}

$phpFile = $route['path'].'/'.$route['fileName'].'.php';
if (!file_exists($phpFile)){
	$phpFile = 'None';
}

$_theme->setVariable('readme', $readme);
$_theme->setVariable('readme_file', $readmeFile);
$_theme->setVariable('php_file', $phpFile);

$_theme->setVariable('USERNAME', $_SESSION['name']);

if($_SESSION['usertype'] != 9){
	$_theme->setVariable('RESTRICTED', 'restricted');
	$_theme->setVariable('DISABLED', 'disabled');
	$_contents->setVariable('DISABLED', 'disabled');
	$_contents->setVariable('RESTRICTED', 'restricted');
}

if($_SESSION['usertype'] == 9){
	$_theme->setCurrentBlock('system');
	$_theme->setVariable('nav-system', 'nav-system');
	$_theme->parse('system');
}

if($_SESSION['usertype'] == 9)
	require_once( dirname(__FILE__) . "/menu/menu.php");