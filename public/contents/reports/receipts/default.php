<?php
require_once(INCLUDEPATH . "fpdf17/fpdf.php");
// Send Headers

$rt_id = isset($_GET['rt_id']) ? $_GET['rt_id'] : 0;
//$rt_id = isset($_POST['rt_id']) ? $_POST['rt_id'] : 0;


$sql = 'SELECT
            rt_id,
            rt_room_id,
            rt_checked_in,
            rt_checked_out,
            rt_extended_hours,
            rt_hourly_rate,
            DATE_FORMAT(rt_checked_in, "%b %d, %Y %h:%i %p") as check_in,
            DATE_FORMAT(rt_checked_out, "%b %d, %Y %h:%i %p") as check_out,
            rt_hours,
            rt_rr_rate,
            rc_qty,
            rc_description,
            rc_rate,
            rt_promo_code,
            rt_promo_rate
        FROM
        tbl_room_transactions
        LEFT JOIN tbl_room_transaction_charges ON rc_room_transaction = rt_id
        LEFT JOIN tbl_users ON u_id = rt_user_id
        WHERE rt_id = ' . $rt_id;
//echo $sql;
$paging = ['currPage'=>$_GET['page'], 'recPerPage'=>10];             // optional parameter to set the returns in pages
$records = $qry->getRecords($sql, $paging);                              // returns associative array [info, data]

function getShift($date, $shifts = array()){
    foreach($shifts as $key => $shift){
        if(strtotime($date) >= strtotime($shift['start']) && strtotime($date) <= strtotime($shift['end']) ){
            return $key+1;
        }
    }

}

$ndata = array();
$rt_id = 0;
$shift = array();

foreach($records['data'] as $key=>$value){
    $temp = array();

    if($value['rt_id'] != $rt_id){
        $temp['rt_id'] = $value['rt_id'];
        $temp['rc_rate'] = $value['rc_rate'];
        $temp['rm_number'] = $value['rt_room_id'];
        $temp['check_in'] = $value['check_in'];
        $temp['check_out'] = $value['check_out'];
        $temp['customer'] = $value['rt_customer_name'];
        $temp['rt_promo_code'] = $value['rt_promo_code'];

        if($value['rt_room_id'] == 999){

        }else{
            if($value['rt_hours'] == 0){
                $temp['promo'] = "";
                $temp['qty'] = $value['rt_extended_hours'];
                $temp['desc'] = $value['rt_extended_hours'] . " HRS - Open Time";
                $temp['amount'] = number_format( $value['rt_hourly_rate'] * $value['rt_extended_hours'], 2);
                $temp['rc_rate'] = $value['rt_hourly_rate'];
                $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;
            }else{
//                $temp['qty'] = $value['rt_hours'];
                $temp['qty'] = 1;
                $temp['desc'] = $value['rt_hours'] . " HRS - Regular";

                $disc = $value['rt_rr_rate'] * $value['rt_promo_rate'];

                $temp['promo'] = $value['rt_promo_code'] ? $value['rt_promo_code'] : "";
                $temp['amount'] = number_format($value['rt_rr_rate'] - $disc, 2 );
                $temp['rc_rate'] = number_format($value['rt_rr_rate'] - $disc, 2 );
                $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;

                if($value['rt_extended_hours'] > 0){
                    $temp['rt_id'] = "";
                    $temp['rm_number'] = "";
                    $temp['check_in'] = "";
                    $temp['check_out'] = "";
                    $temp['customer'] = "";
                    $temp['promo'] = "";

                    $temp['qty'] = $value['rt_extended_hours'];
                    $temp['desc'] = "Extended Hours";
                    $temp['rc_rate'] = $value['rt_hourly_rate'];
                    $temp['amount'] = number_format($value['rt_hourly_rate'] * $value['rt_extended_hours'], 2);
                    $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;
                }
            }
        }

        if($value['rc_qty'] > 0){
            if($value['rt_room_id'] != 999){
                $temp['rt_id'] = "";
                $temp['rm_number'] = "";
                $temp['check_in'] = "";
                $temp['check_out'] = "";
                $temp['customer'] = "";
                $temp['promo'] = "";
            }

            $temp['qty'] = $value['rc_qty'];
            $temp['desc'] = $value['rc_description'];
            $temp['rc_rate'] = $value['rc_rate'];
            $temp['amount'] = number_format( $value['rc_rate'], 2 );
            $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;
        }

        $rt_id = $value['rt_id'];
    }else{
        $temp['rm_number'] = "";
        $temp['check_in'] = "";
        $temp['check_out'] = "";

        $temp['qty'] = $value['rc_qty'];
        $temp['desc'] = $value['rc_description'];
        $temp['amount'] = number_format( $value['rc_rate'], 2 );
        $temp['rc_rate'] = $value['rc_rate'];
        $ndata[ getShift($value['rt_checked_out'], $shifts) ][] = $temp;
    }
}


class PDF extends FPDF
{
// Page header

    var $date_start = "";
    var $date_end = "";
    var $header = "";
    var $width = "";
    var $align = "";
    var $shifts = array();

    function Header()
    {
        $this->SetFont('Arial','B',11);
        $this->Cell(0,1,'Mamay Road, Brgy Angliongto, Davao City',0,1,'C');
        $this->Cell(0,8,'Tel. No. (082) 305-5936',0,1,'C');

        // Logo
        $this->Image('images/Mamaylogo.png',10,6,30);
        $this->SetFont('Arial','B',10);
        $this->Ln();
        $this->Ln();
        //$this->Cell(0,6, 'From: ' .$this->date_start . ' To: ' . $this->date_end,0,1,'C');
        // Header
        $this->SetFont('Arial','B',8);
        /*foreach($this->header as $key=>$col)
            $this->Cell($this->width[$key],7,$col,'TB', 0, $this->align[$key]);
        $this->Ln();*/
    }

// Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,$this->PageNo(),0,0,'C');
    }


// Better table
    function BasicTable($header, $width, $data, $align, $tot)
    {
        // Data
        $grand_total = 0;
        foreach($data as $key=>$shift){
            /*$this->Cell($width[0],6,$this->ordinal($key). " Shift",0,0, $align[0]);
            $this->Cell($width[1],6,"",0,0, $align[1]);
            $this->Cell($width[2],6,"",0,0, $align[2]);
            $this->Cell($width[3],6,"",0,0, $align[3]);
            $this->Cell($width[4],6,"",0,0, $align[4]);
            $this->Cell($width[5],6,"",0,0, $align[5]);
            $this->Cell($width[0],6,"",0,0, $align[6]);
            $this->Ln();*/

            $sub_grand_total = 0;
            foreach($shift as $key=>$row)
            {

                if($key == 0){
                    $this->SetFont('Arial','B',9);
                    $this->Cell(40,6,'Reference Number',0,0, $align[0]);
                    $this->SetFont('Arial','',9);
                    $this->Cell(50,6,str_pad($row['rt_id'], 6, "0", STR_PAD_LEFT),'B',0, $align[0]);
                    $this->Ln();

                    $this->SetFont('Arial','B',9);
                    $this->Cell(40,6,'Room Number',0,0, $align[0]);
                    $this->SetFont('Arial','',9);
                    $this->Cell(50,6,$row['rm_number'],'B',0, $align[0]);
                    $this->Ln();

                    $this->SetFont('Arial','B',9);
                    $this->Cell(40,6,'Date In',0,0, $align[0]);
                    $this->SetFont('Arial','',9);
                    $this->Cell(50,6,$row['check_in'],'B',0, $align[0]);
                    $this->Ln();

                    $this->SetFont('Arial','B',9);
                    $this->Cell(40,6,'Date Out',0,0, $align[0]);
                    $this->SetFont('Arial','',9);
                    $this->Cell(50,6,$row['check_out'],'B',0, $align[0]);
                    $this->Ln();

                    $this->SetFont('Arial','B',9);
                    $this->Cell(40,6,'Promo Code',0,0, $align[0]);
                    $this->SetFont('Arial','',9);
                    $this->Cell(50,6,$row['rt_promo_code'],'B',0, $align[0]);
                    $this->Ln();

                    $this->Ln();
                    $this->Ln();

                    $this->SetFont('Arial','B',9);
                    $this->Cell(20,6,"Qty",'TB', 0, 'L');
                    $this->Cell(120,6,"Description",'TB', 0, 'L');
                    $this->Cell(28,6,"Rate",'TB', 0, 'R');
                    $this->Cell(30,6,"Sub Total",'TB', 0, 'R');
                    $this->Ln();
                }


                $this->SetFont('Arial','',9);
                $this->Cell(20,6,$row['qty'],'', 0, 'L');
                $this->Cell(120,6,$row['desc'],'', 0, 'L');
                $this->Cell(28,6,$row['rc_rate'],'', 0, 'R');
                if(strpos($row['desc'], 'HRS')){
                    $this->Cell(30,6,number_format( $row['rc_rate'], 2),'', 0, 'R');
                }else{
                    $this->Cell(30,6,number_format( $row['qty'] * $row['rc_rate'], 2),'', 0, 'R');
                }
                $this->Ln();


                $grand_total += str_replace(",","",$row['amount']);
                $sub_grand_total += str_replace(",","",$row['amount']);
            }

        }

        /*$this->SetFont('Arial','B',8);
        $this->Cell($width[0],6,"",0,0, $align[0]);
        $this->Cell($width[1],6,"",0,0, $align[1]);
        $this->Cell($width[2],6,"",0,0, $align[2]);
        $this->Cell($width[3],6,"",0,0, $align[3]);
        $this->Cell($width[4],6,"",0,0, $align[4]);
        $this->Cell($width[5],6,"",0,0, $align[5]);
        $this->Cell($width[6],6,number_format($grand_total, 2),'T', 0, $align[6]);
*/

        $this->Cell(20,6,"",'T', 0, 'L');
        $this->Cell(120,6,"",'T', 0, 'L');
        $this->Cell(28,6,"",'T', 0, 'R');
        $this->Cell(30,6,"",'T', 0, 'R');
        $this->Ln();

        $this->Cell(20,6,"",'', 0, 'L');
        $this->Cell(120,6,"",'', 0, 'L');
        $this->Cell(28,6,"Tax Base",'', 0, 'R');
        $this->Cell(30,6,number_format( $grand_total - ($grand_total * 0.12), 2),'', 0, 'R');
        $this->Ln();

        $this->Cell(20,6,"",'', 0, 'L');
        $this->Cell(120,6,"",'', 0, 'L');
        $this->Cell(28,6,"12% VAT",'', 0, 'R');
        $this->Cell(30,6,number_format( ($grand_total * 0.12), 2),'', 0, 'R');
        $this->Ln();

        $this->Cell(20,6,"",'', 0, 'L');
        $this->Cell(120,6,"",'', 0, 'L');
        $this->Cell(28,6,"TOTAL PAYMENT",'', 0, 'R');
        $this->Cell(30,6,number_format( $grand_total, 2),'', 0, 'R');
        $this->Ln();

        $this->Ln();
        $this->Ln();
        $this->Ln();
        $this->Ln();
        $this->Cell(80,6,'Cashier','T',0, 'C');
        $custs = $this->getCustomerByShift($data);
        $this->Ln();

    }


    function getCustomerByShift($data){

        $tot_customer = array();
        foreach($this->shifts as $key=>$shift){
            if(isset($data[$key+1])){
                $cnt = 0;
                foreach($data[$key+1] as $row){
                    if($row['rm_number'] != "")
                        $cnt++;
                }
                $tot_customer[] = $cnt;
            }else{
                $tot_customer[] = 0;
            }

        }

        return $tot_customer;
    }

    function ordinal($number) {
        $ends = array('th','st','nd','rd','th','th','th','th','th','th');
        if ((($number % 100) >= 11) && (($number%100) <= 13))
            return $number. 'th';
        else
            return $number. $ends[$number % 10];
    }

    function getSalesByShift($data){

        $tot_sales = array();
        foreach($this->shifts as $key=>$shift){
            if(isset($data[$key+1])){
                $cnt = 0;
                foreach($data[$key+1] as $row){
                    $cnt += str_replace(",","",$row['amount']);
                }
                $tot_sales[] = $cnt;
            }else{
                $tot_sales[] = 0;
            }

        }

        return $tot_sales;
    }
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();


$header = array('Room #', 'Check-In', 'Check-Out', 'VIP #', 'Qty', 'Description', 'Amount');
$width = array(15, 40, 40, 15, 15, 40, 30);
$align = array('L', 'L', 'L', 'L', 'L', 'L', 'R');
// Data loading

$pdf->date_start = date('F j, Y, g:i A', strtotime($startDate) );
$pdf->date_end = date('F j, Y, g:i A', strtotime($endDate) );
$pdf->header = $header;
$pdf->align = $align;
$pdf->width = $width;
$pdf->shifts = $shifts;

$pdf->AddPage('P', 'Letter');
$pdf->SetFont('Times','',12);
$pdf->BasicTable($header,$width,$ndata, $align, $tot);

//$pdf->Output('CollectionReport.pdf', 'D');
$pdf->Output();