<?php

//print_r($_GET);

$where = "";
$sort = "rpt_checked_in";

if(isset($_GET['ref_num'])){
    $where .= "where rt_id = " . intval($_GET['ref_num']);
}else{
    if(isset($_GET['date_range'])){
        /*$range = explode(" - ", $_GET['date_range']);
        $where .= " WHERE rt_checked_out >= '" . date('Y-m-d', strtotime($range[0]) ) . " 00:00:00' AND rt_checked_out <= '" . date('Y-m-d', strtotime($range[1]) ) . " 23:59:59'";
        $filename = "Summary_Reports-" . $range[0] . '_to_' . $range[1] . '.xlsx';*/

        $date = $_GET['date_range'];
        $where .= " WHERE rt_checked_out >= '" . date('Y-m-d', strtotime($date) ) . " 08:00:00' AND rt_checked_out <= '" . date('Y-m-d', strtotime($date  . '+1 day') ) . " 08:00:00'";
        $filename = "Summary_Reports-" . date('Y-m-d', strtotime($date) ) . '.xlsx';
    }else{
        $where .= " WHERE rt_checked_out >= '" . date('Y-m-d') . " 08:00:00' AND rt_checked_out <= '" . date('Y-m-d', strtotime( date('Y-m-d') . '+1 day')) . " 08:00:00'";
        $filename = "Summary_Reports-" . date('Y-m-d') . '.xlsx';
    }


    if(isset($_GET['sort'])){
        if($_GET['sort'] != ''){
            $where .= " AND rpt_user_id = " . $_GET['sort'];
        }
    }
}

$sql = 'SELECT
            rt_id,
            rt_room_id,
            rt_checked_in,
            rt_checked_out,
            rt_customer_name,
            rt_extended_hours,
            rt_customer_name,
            rt_hourly_rate,
            DATE_FORMAT(rt_checked_in, "%b %d, %Y %h:%i %p") as check_in,
            DATE_FORMAT(rt_checked_out, "%b %d, %Y %h:%i %p") as check_out,
            rt_hours,
            rt_rr_rate,
            rc_qty,
            rc_qty * rc_rate as subtotal,
            rc_description,
            rc_rate,
            rt_promo_code,
            rt_promo_rate
        FROM
        tbl_room_transactions
        LEFT JOIN tbl_room_transaction_charges ON rc_room_transaction = rt_id
        LEFT JOIN tbl_users ON u_id = rt_user_id
        ' . $where . '
        ORDER BY rt_checked_out ASC';
//echo $sql;
$paging = ['currPage'=>$_GET['page'], 'recPerPage'=>30];             // optional parameter to set the returns in pages
$records = $qry->getRecords($sql, $paging);                              // returns associative array [info, data]

$ndata = array();
$rt_id = 0;
//str_pad($row['rt_id'], 6, "0", STR_PAD_LEFT)
foreach($records['data'] as $key=>$value){
    $temp = array();
    if($value['rt_id'] != $rt_id){
        $temp['rt_id'] = str_pad($value['rt_id'], 6, "0", STR_PAD_LEFT);
        $temp['pdf'] = '<a href="{SITEURL}manage/checked-in/house_account/?id=999&rt_id=' . $value['rt_id'] . '" class="display-modal-pdf" data-type="pdf" title="Order Transaction"><i class="fa fa-file-pdf-o" style="color: forestgreen;"></i></a> ';
        $temp['rm_number'] = $value['rt_room_id'];
        $temp['check_in'] = $value['check_in'];
        $temp['check_out'] = $value['check_out'];
        $temp['customer'] = $value['rt_customer_name'];

        if($value['rt_room_id'] == 999){

        }else{
            if($value['rt_hours'] == 0){
                $temp['promo'] = "";
                $temp['qty'] = $value['rt_extended_hours'];
                $temp['desc'] = $value['rt_extended_hours'] . " HRS - Open Time";
                $temp['amount'] = number_format( $value['rt_hourly_rate'] * $value['rt_extended_hours'], 2);
                $temp['subtotal'] = $value['subtotal'];
                $ndata[] = $temp;
            }else{
                //$temp['qty'] = $value['rt_hours'];
                $temp['qty'] = 1;
                $temp['desc'] = $value['rt_hours'] . " HRS - Regular";

                $disc = $value['rt_rr_rate'] * $value['rt_promo_rate'];

                $temp['promo'] = $value['rt_promo_code'] ? $value['rt_promo_code'] : "";
                $temp['amount'] = number_format($value['rt_rr_rate'] - $disc, 2 );
                $temp['subtotal'] = number_format($value['rt_rr_rate'] - $disc, 2 );
                $ndata[] = $temp;

                if($value['rt_extended_hours'] > 0 && $value['rt_hours'] > 0){
                    $temp['rt_id'] = "";
                    $temp['pdf'] = "";
                    $temp['rm_number'] = "";
                    $temp['check_in'] = "";
                    $temp['check_out'] = "";
                    $temp['customer'] = "";
                    $temp['promo'] = "";

                    $temp['qty'] = $value['rt_extended_hours'];
                    $temp['desc'] = "Extended Hours";
                    $temp['amount'] = number_format($value['rt_hourly_rate'], 2);
                    $temp['subtotal'] = number_format($value['rt_hourly_rate'] * $value['rt_extended_hours'], 2);
                    $ndata[] = $temp;
                }
            }
        }

        if($value['rc_qty'] > 0){
            if($value['rt_room_id'] != 999){
                $temp['rt_id'] = "";
                $temp['pdf'] = "";
                $temp['rm_number'] = "";
                $temp['check_in'] = "";
                $temp['check_out'] = "";
                $temp['customer'] = "";
                $temp['promo'] = "";
            }

            $temp['qty'] = $value['rc_qty'];
            $temp['desc'] = $value['rc_description'];
            $temp['amount'] = number_format( $value['rc_rate'], 2 );
            $temp['subtotal'] = $value['subtotal'];
            $ndata[] = $temp;
        }

        $rt_id = $value['rt_id'];
    }else{
        $temp['rt_id'] = "";
        $temp['pdf'] = "";
        $temp['rm_number'] = "";
        $temp['check_in'] = "";
        $temp['check_out'] = "";
        $temp['customer'] = "";

        $temp['qty'] = $value['rc_qty'];
        $temp['desc'] = $value['rc_description'];
        $temp['amount'] = number_format( $value['rc_rate'], 2 );
        $temp['subtotal'] = $value['subtotal'];
        $ndata[] = $temp;
    }
}

$records['data'] = $ndata;
$app->json_encode($records);


