/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : mamayinn

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-05-30 12:21:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_category_charges
-- ----------------------------
DROP TABLE IF EXISTS `tbl_category_charges`;
CREATE TABLE `tbl_category_charges` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_description` varchar(100) DEFAULT NULL,
  `cat_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `cat_deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_category_charges
-- ----------------------------
INSERT INTO `tbl_category_charges` VALUES ('1', 'Testing', '1', null);
INSERT INTO `tbl_category_charges` VALUES ('2', 'Short Order', '0', null);
INSERT INTO `tbl_category_charges` VALUES ('3', 'Sandwiches', '0', null);
INSERT INTO `tbl_category_charges` VALUES ('4', 'Breakfast', '0', null);
INSERT INTO `tbl_category_charges` VALUES ('5', 'Pampulutan', '0', null);
INSERT INTO `tbl_category_charges` VALUES ('6', 'Others', '0', null);
INSERT INTO `tbl_category_charges` VALUES ('7', 'Telephone', '1', null);
INSERT INTO `tbl_category_charges` VALUES ('8', 'Beverages', '0', null);
INSERT INTO `tbl_category_charges` VALUES ('9', 'Red Bull', '1', null);

-- ----------------------------
-- Table structure for tbl_extra_charges
-- ----------------------------
DROP TABLE IF EXISTS `tbl_extra_charges`;
CREATE TABLE `tbl_extra_charges` (
  `ec_id` int(11) NOT NULL AUTO_INCREMENT,
  `ec_cat_id` int(11) DEFAULT NULL,
  `ec_name` varchar(100) DEFAULT NULL,
  `ec_price` decimal(11,2) DEFAULT NULL,
  `ec_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `ec_deleted_date` datetime DEFAULT NULL,
  `ec_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_extra_charges
-- ----------------------------
INSERT INTO `tbl_extra_charges` VALUES ('1', '2', 'Grilled Porkchop', '135.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('2', '2', 'Breaded Porkchop', '135.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('3', '2', 'Beef w/ broccoli', '180.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('4', '2', 'Beef w/ ampalaya', '180.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('5', '2', 'Bistek Tagalog', '180.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('6', '2', 'Grilled Malasugue', '180.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('7', '2', 'Sinigang baboy / Malasugue', '180.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('8', '2', 'Tinolang Manok / Malasugue', '180.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('9', '2', 'Chicken & Prok Adobo', '180.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('10', '2', 'Kamatis & Itlog Maalat', '60.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('11', '2', 'Dilis Salad', '65.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('12', '2', 'Lumpia Shanghai', '85.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('13', '2', 'Kinilaw malasugue', '180.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('14', '2', 'Buttered Chicken', '150.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('15', '2', 'Fried Chicken', '150.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('16', '2', 'Lomi', '85.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('17', '2', 'Bihon / Sotanghon G.', '85.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('18', '3', 'Ham & Cheese', '65.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('19', '3', 'Tuna Sandwich', '65.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('20', '3', 'Chicken Sandwich', '65.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('21', '3', 'Hotdog Sandwich', '65.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('22', '3', 'Ham Burger', '85.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('23', '3', 'Cheeses Burger', '95.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('24', '3', 'Spam Sandwich', '115.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('25', '3', 'CLUBHOUSE', '100.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('26', '4', 'Spamsilog', '115.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('27', '4', 'Tosilog', '75.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('28', '4', 'Losilog', '75.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('29', '4', 'Hotsilog', '75.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('30', '4', 'Cornsilog', '85.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('31', '4', 'Chicken Adobo Toppings', '69.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('32', '4', 'Pork Adobo Toppings', '69.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('33', '4', 'Bistek Toppings', '69.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('34', '5', 'Mani', '30.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('35', '5', 'Sisig', '140.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('36', '5', 'Kropik', '60.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('37', '5', 'Calamares', '110.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('38', '5', 'F Fries', '60.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('39', '5', 'Fish Finger', '110.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('40', '5', 'Dilis', '65.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('41', '5', 'Lechon Kawali', '85.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('42', '8', 'Pepsi', '45.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('43', '8', '7up', '45.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('44', '8', 'Water', '30.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('45', '8', 'Canned Juices', '45.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('46', '8', 'Icetea', '30.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('47', '8', 'Lipovitan', '50.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('48', '8', 'Coffee', '25.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('49', '8', 'Apple flavored', '55.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('50', '8', 'Redhorse', '55.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('51', '6', 'Plain Rice', '30.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('52', '6', 'Garlic Rice', '40.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('53', '6', 'Fried Rice', '40.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('54', '6', 'Condom ( Trust )', '30.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('55', '6', 'Condom ( Premiere )', '40.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('56', '6', 'Lubricant', '50.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('57', '6', 'Guest Kit', '30.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('58', '6', 'Malboro', '50.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('59', '6', 'Extra Towel', '50.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('60', '6', 'Extra Blanket', '50.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('61', '6', 'Extra Person', '250.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('62', '6', 'Extra Pillow', '50.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('63', '6', 'Stain', '50.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('64', '6', 'Marlboro', '50.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('65', '8', 'san mig light', '55.00', '0', null, '1');
INSERT INTO `tbl_extra_charges` VALUES ('66', '8', 'palesin', '55.00', '0', null, '1');

-- ----------------------------
-- Table structure for tbl_promos
-- ----------------------------
DROP TABLE IF EXISTS `tbl_promos`;
CREATE TABLE `tbl_promos` (
  `prm_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `prm_code` varchar(10) DEFAULT NULL,
  `prm_rm_type` int(11) DEFAULT NULL,
  `prm_date_created` datetime DEFAULT NULL,
  `prm_status` tinyint(4) DEFAULT NULL,
  `prm_created_by` int(11) DEFAULT NULL,
  `prm_expiration` datetime DEFAULT NULL,
  `prm_discount` decimal(11,2) DEFAULT NULL,
  `prm_discount_percentage` decimal(11,2) DEFAULT NULL,
  `prm_deleted` tinyint(4) DEFAULT NULL,
  `prm_deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`prm_id`),
  UNIQUE KEY `promo_code` (`prm_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_promos
-- ----------------------------
INSERT INTO `tbl_promos` VALUES ('1', 'DLQKQKUZCH', '1', null, '1', null, null, '0.00', '0.15', null, null);
INSERT INTO `tbl_promos` VALUES ('2', '123456', '1', null, '1', null, null, '0.00', '0.15', null, null);

-- ----------------------------
-- Table structure for tbl_reports
-- ----------------------------
DROP TABLE IF EXISTS `tbl_reports`;
CREATE TABLE `tbl_reports` (
  `rpt_id` int(11) NOT NULL AUTO_INCREMENT,
  `rpt_rm_number` int(11) DEFAULT NULL,
  `rpt_rm_type` varchar(50) DEFAULT NULL,
  `rpt_checked_in` datetime DEFAULT NULL,
  `rpt_checked_out` datetime DEFAULT NULL,
  `rpt_hours` int(11) DEFAULT NULL,
  `rpt_room_charge` decimal(10,0) DEFAULT NULL,
  `rpt_extra_charge` decimal(10,0) DEFAULT NULL,
  `rpt_rt_id` int(11) DEFAULT NULL,
  `rpt_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`rpt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_reports
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_rooms
-- ----------------------------
DROP TABLE IF EXISTS `tbl_rooms`;
CREATE TABLE `tbl_rooms` (
  `rm_id` int(11) NOT NULL AUTO_INCREMENT,
  `rm_number` int(11) NOT NULL,
  `rm_type` int(11) NOT NULL,
  `rm_status` tinyint(4) DEFAULT '1' COMMENT '0 = Occupied, 1 = Vacant, 2 = Reserved,  3 = House Account, 4 = Repairs',
  `rm_deleted` tinyint(4) DEFAULT NULL,
  `rm_deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`rm_id`),
  UNIQUE KEY `room_id` (`rm_id`,`rm_number`),
  KEY `room types` (`rm_type`),
  CONSTRAINT `room types` FOREIGN KEY (`rm_type`) REFERENCES `tbl_room_types` (`rtp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_rooms
-- ----------------------------
INSERT INTO `tbl_rooms` VALUES ('101', '101', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('102', '102', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('103', '103', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('104', '104', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('105', '105', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('106', '106', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('107', '107', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('108', '108', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('109', '109', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('110', '110', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('111', '111', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('112', '112', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('113', '113', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('114', '114', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('115', '115', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('116', '116', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('117', '117', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('118', '118', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('119', '119', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('120', '120', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('121', '121', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('122', '122', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('123', '123', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('124', '124', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('125', '125', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('126', '126', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('127', '127', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('128', '128', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('129', '129', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('130', '130', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('131', '131', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('132', '132', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('133', '133', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('134', '134', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('135', '135', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('136', '136', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('137', '137', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('138', '138', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('139', '139', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('140', '140', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('141', '141', '1', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('142', '142', '6', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('143', '143', '6', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('144', '144', '6', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('145', '145', '6', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('146', '146', '6', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('147', '147', '6', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('148', '148', '6', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('149', '149', '6', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('150', '150', '6', '1', null, null);
INSERT INTO `tbl_rooms` VALUES ('151', '200', '5', '1', null, null);

-- ----------------------------
-- Table structure for tbl_room_rates
-- ----------------------------
DROP TABLE IF EXISTS `tbl_room_rates`;
CREATE TABLE `tbl_room_rates` (
  `rr_id` int(11) NOT NULL AUTO_INCREMENT,
  `rr_room_day` int(11) DEFAULT NULL,
  `rr_room_type` int(11) DEFAULT NULL,
  `rr_hours` int(11) DEFAULT NULL,
  `rr_rate` decimal(11,2) DEFAULT NULL,
  `rr_excess_rate` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`rr_id`),
  UNIQUE KEY `room_hours` (`rr_room_type`,`rr_hours`,`rr_room_day`) USING BTREE,
  KEY `room_rates` (`rr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_room_rates
-- ----------------------------
INSERT INTO `tbl_room_rates` VALUES ('11', '1', '1', '3', '350.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('12', '2', '1', '3', '350.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('13', '3', '1', '3', '350.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('14', '4', '1', '3', '350.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('15', '5', '1', '3', '375.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('16', '6', '1', '3', '375.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('17', '7', '1', '3', '375.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('18', '1', '1', '12', '700.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('19', '2', '1', '12', '700.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('20', '3', '1', '12', '700.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('21', '4', '1', '12', '700.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('22', '5', '1', '12', '800.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('23', '6', '1', '12', '800.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('24', '7', '1', '12', '800.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('25', '1', '1', '24', '1200.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('26', '2', '1', '24', '1200.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('27', '3', '1', '24', '1200.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('28', '4', '1', '24', '1200.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('29', '5', '1', '24', '1400.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('30', '6', '1', '24', '1400.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('31', '7', '1', '24', '1400.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('32', '1', '6', '3', '275.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('33', '2', '6', '3', '275.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('34', '3', '6', '3', '275.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('35', '4', '6', '3', '275.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('36', '5', '6', '3', '300.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('37', '6', '6', '3', '300.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('38', '7', '6', '3', '300.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('39', '1', '6', '12', '600.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('40', '2', '6', '12', '600.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('41', '3', '6', '12', '600.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('42', '4', '6', '12', '600.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('43', '5', '6', '12', '700.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('44', '6', '6', '12', '700.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('45', '7', '6', '12', '700.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('46', '1', '6', '24', '1000.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('47', '2', '6', '24', '1000.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('48', '3', '6', '24', '1000.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('49', '4', '6', '24', '1000.00', '80.00');
INSERT INTO `tbl_room_rates` VALUES ('50', '5', '6', '24', '1200.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('51', '6', '6', '24', '1200.00', '100.00');
INSERT INTO `tbl_room_rates` VALUES ('52', '7', '6', '24', '1200.00', '100.00');

-- ----------------------------
-- Table structure for tbl_room_transactions
-- ----------------------------
DROP TABLE IF EXISTS `tbl_room_transactions`;
CREATE TABLE `tbl_room_transactions` (
  `rt_id` int(11) NOT NULL AUTO_INCREMENT,
  `rt_room_id` int(11) DEFAULT NULL,
  `rt_checked_in` datetime DEFAULT NULL,
  `rt_checked_out` datetime DEFAULT NULL,
  `rt_checked_out_by` int(11) DEFAULT NULL,
  `rt_date_created` datetime DEFAULT NULL,
  `rt_user_id` int(11) DEFAULT NULL,
  `rt_hourly_rate` decimal(11,0) DEFAULT NULL,
  `rt_hours` int(11) DEFAULT NULL,
  `rt_extended_hours` int(11) DEFAULT NULL,
  `rt_status` tinyint(1) DEFAULT '0',
  `rt_customer_name` varchar(50) DEFAULT NULL,
  `rt_promo_id` int(11) DEFAULT NULL,
  `rt_promo_code` varchar(10) DEFAULT NULL,
  `rt_promo_rate` decimal(11,2) DEFAULT NULL,
  `rt_rr_id` int(11) DEFAULT NULL,
  `rt_rr_rate` decimal(11,2) DEFAULT NULL,
  `rt_date_modified` datetime DEFAULT NULL,
  `rt_modified_by` int(11) DEFAULT NULL,
  `rt_canceled` tinyint(4) DEFAULT NULL,
  `rt_canceled_by` int(11) DEFAULT NULL,
  `rt_v_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`rt_id`),
  KEY `room` (`rt_room_id`),
  KEY `room_transaction` (`rt_id`),
  KEY `user` (`rt_user_id`),
  CONSTRAINT `room` FOREIGN KEY (`rt_room_id`) REFERENCES `tbl_rooms` (`rm_id`),
  CONSTRAINT `user` FOREIGN KEY (`rt_user_id`) REFERENCES `tbl_users` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_room_transactions
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_room_transaction_charges
-- ----------------------------
DROP TABLE IF EXISTS `tbl_room_transaction_charges`;
CREATE TABLE `tbl_room_transaction_charges` (
  `rc_id` int(11) NOT NULL AUTO_INCREMENT,
  `rc_room_transaction` int(11) DEFAULT NULL,
  `rc_ec_id` int(11) DEFAULT NULL,
  `rc_qty` int(11) DEFAULT NULL,
  `rc_description` varchar(100) DEFAULT NULL,
  `rc_rate` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`rc_id`),
  KEY `room_transactions` (`rc_room_transaction`),
  KEY `room_charges` (`rc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_room_transaction_charges
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_room_types
-- ----------------------------
DROP TABLE IF EXISTS `tbl_room_types`;
CREATE TABLE `tbl_room_types` (
  `rtp_id` int(11) NOT NULL AUTO_INCREMENT,
  `rtp_name` varchar(100) DEFAULT NULL,
  `rtp_hourly_rate` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`rtp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_room_types
-- ----------------------------
INSERT INTO `tbl_room_types` VALUES ('1', 'Standard Room', '100.00');
INSERT INTO `tbl_room_types` VALUES ('2', 'VIP', '200.00');
INSERT INTO `tbl_room_types` VALUES ('3', 'Deluxe Room', '300.00');
INSERT INTO `tbl_room_types` VALUES ('5', 'Inventory Room', '0.00');
INSERT INTO `tbl_room_types` VALUES ('6', 'Mini Room', '100.00');

-- ----------------------------
-- Table structure for tbl_users
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_firstname` varchar(50) DEFAULT NULL,
  `u_middlename` varchar(50) DEFAULT NULL,
  `u_lastname` varchar(50) DEFAULT NULL,
  `u_extname` varchar(10) DEFAULT NULL,
  `u_birthdate` date DEFAULT NULL,
  `u_gender` tinyint(4) DEFAULT NULL COMMENT '0 = male; 1 = female',
  `u_usertype` int(4) DEFAULT NULL COMMENT '0 = Clerk; 9 = super admin',
  `u_username` varchar(50) DEFAULT NULL,
  `u_password` varchar(100) DEFAULT NULL,
  `u_deleted` tinyint(4) DEFAULT NULL,
  `u_deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `username` (`u_username`),
  KEY `user_id` (`u_id`) USING BTREE,
  KEY `utypes` (`u_usertype`),
  CONSTRAINT `utypes` FOREIGN KEY (`u_usertype`) REFERENCES `tbl_usertypes` (`ut_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
INSERT INTO `tbl_users` VALUES ('1', 'Joseph', 'D', 'Sarsaba', '', '2015-08-12', '0', '9', 'jsarsaba', 'c34ae4ef6bc7c574d88d225d1d7489f8', null, null);
INSERT INTO `tbl_users` VALUES ('39', 'Arbaina', 'Abi', 'Bangin', '', '0000-00-00', '1', '1', 'abangin', '8c249675aea6c3cbd91661bbae767ff1', null, null);
INSERT INTO `tbl_users` VALUES ('40', 'Marjorie', 'V', 'Ronda', '', '1993-01-11', '1', '1', 'mronda', 'de1e3b0952476aae6888f98ea0e4ac11', null, null);
INSERT INTO `tbl_users` VALUES ('41', 'Shedar jean', 'J', 'Maestre', '', '1992-01-02', '1', '1', 'smaestre', '52f38e276849b098919cc0b53d9d360a', null, null);
INSERT INTO `tbl_users` VALUES ('42', 'Allan', 'S', 'Cabusora', '', '0000-00-00', '0', '9', 'ascex', 'bc3a38d218e4c77cabe56d7a94ff51eb', null, null);
INSERT INTO `tbl_users` VALUES ('43', 'Admin', '', '', '', '0000-00-00', '0', '9', 'admin', '21232f297a57a5a743894a0e4a801fc3', null, null);

-- ----------------------------
-- Table structure for tbl_usertypes
-- ----------------------------
DROP TABLE IF EXISTS `tbl_usertypes`;
CREATE TABLE `tbl_usertypes` (
  `ut_id` int(11) NOT NULL AUTO_INCREMENT,
  `ut_name` varchar(100) DEFAULT NULL,
  `ut_deleted` tinyint(4) DEFAULT NULL,
  `ut_deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ut_id`),
  KEY `usertype` (`ut_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_usertypes
-- ----------------------------
INSERT INTO `tbl_usertypes` VALUES ('1', 'Clerk', null, null);
INSERT INTO `tbl_usertypes` VALUES ('9', 'Administrator', null, null);

-- ----------------------------
-- Table structure for tbl_vehicles
-- ----------------------------
DROP TABLE IF EXISTS `tbl_vehicles`;
CREATE TABLE `tbl_vehicles` (
  `v_id` int(11) NOT NULL AUTO_INCREMENT,
  `v_name` varchar(100) DEFAULT NULL,
  `v_plate_no` varchar(12) DEFAULT NULL,
  `v_deleted` tinyint(4) DEFAULT NULL,
  `v_deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`v_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_vehicles
-- ----------------------------
INSERT INTO `tbl_vehicles` VALUES ('1', 'Honda', 'ABC123', null, null);
INSERT INTO `tbl_vehicles` VALUES ('2', 'Toyota', 'EDF456', null, null);
INSERT INTO `tbl_vehicles` VALUES ('3', 'Volks', 'GHI789', null, null);
